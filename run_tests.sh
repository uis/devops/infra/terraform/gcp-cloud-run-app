#! /usr/bin/env bash

set -e
shopt -s expand_aliases

usage() {
    cat << EOF
Usage:
    ./run_tests.sh
    ./run_tests.sh -t tests/default.tftest.hcl
    ./run_tests.sh -t tests/default.tftest.hcl -t tests/cloudsql.tftest.hcl

Options:
    -c                      Run in CI/CD job mode meaning that terraform is executed directly, rather than via
                            docker compose.
    -t <test file path>     Execute specific test files only (all tests are executed by default).
                            This option can be specified multiple times.
    -v                      Add the verbose switch to the terraform test command.
    -h                      Display this help message.
EOF
}

# The sed commands in this script must use gnu-sed on mac as the default sed on MacOS is a steaming pile of...
if [[ "$(uname)" == "Darwin" ]]; then
    if [ ! "$(command -v gsed)" ]; then
        echo -e "This script requires gnu-sed on Mac OS. Install it with 'brew install gnu-sed'"
        exit 1
    fi
    alias sed="gsed"
fi

cleanup() {
    # Uncomment the prevent_destroy lifecycle argument if it has been commented out by the command further in the
    # script.
    sed -i 's/^    # prevent_destroy = true/    prevent_destroy = true/g' static_egress_ip.tf

    # Remove the lockfile file as it is only necessary to enable the test run.
    if [[ -f .terraform.lock.hcl ]]; then
        rm .terraform.lock.hcl
    fi

    # Ensure the original versions.tf.json is restored.
    if [[ -f versions.tf.json.bak ]]; then
        mv versions.tf.json.bak versions.tf.json
    fi
}

trap 'cleanup' EXIT INT TERM

ci_run=false
verbose=false
tests=()
all_tests=true  # Default to true if no -t options are provided

while getopts ":cht:v" opt; do
  case $opt in
    c)
      ci_run=true
      ;;
    h)
      usage
      exit
      ;;
    t)
      if [ -z "$OPTARG" ] || [[ "$OPTARG" == -* ]]; then
        echo "Error: Option -t requires a non-empty argument." >&2
        exit 1
      fi
      tests+=("$OPTARG")
      all_tests=false
      ;;
    v)
      verbose=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done
shift "$((OPTIND - 1))"

test_args=("--var-file=tests/tests.tfvars")

if [[ "${#tests[@]}" -gt 0 ]]; then
    for test in "${tests[@]}"; do
        test_args+=("--filter=$test")
    done
fi

if [ $verbose = true ]; then
    test_args+=(--verbose)
    echo "CI mode: $ci_run"
    echo "Verbose: $verbose"
fi

# Comment out the prevent_destroy lifecycle argument otherwise the tests will fail as they cannot tear down the
# resource.
sed -i 's/^    prevent_destroy = true/    # prevent_destroy = true/g' static_egress_ip.tf

# The terraform testing framework doesn't yet have a nice way to target specific versions of a provider. Therefore, we
# are having to programatically change the Google provider versions in the versions.tf.json file for the time being.
if [[ -n $GOOGLE_PROVIDER_VERSION_CONSTRAINT ]]; then
    mv versions.tf.json versions.tf.json.bak
    jq ".terraform[0].required_providers[0].google.version |= \"$GOOGLE_PROVIDER_VERSION_CONSTRAINT\"" versions.tf.json.bak \
        > versions.tf.json
fi

if [ "$ci_run" = true ]; then
    terraform init
    terraform test "${test_args[@]}"
else
    docker compose run --rm test "$(cat << EOF
terraform init
terraform test ${test_args[@]}
EOF
    )"
fi
