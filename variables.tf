# Cloud Run service variables
variable "name" {
  description = "Prefix used to form resource names."
  type        = string
  default     = "webapp"

  validation {
    condition     = length(var.name) <= 18
    error_message = "var.name cannot be greater than 18 characters."
  }
}

variable "region" {
  description = "Location used to create Cloud Run service and other resources."
  type        = string
}

variable "description" {
  description = "A description for the Cloud Run service."
  type        = string
  default     = null
}

variable "project" {
  description = "Project containing the webapp."
  type        = string
}

variable "service_labels" {
  description = <<EOI
A set of key/value label pairs to assign to the Cloud Run service.
EOI
  type        = map(string)
  default     = {}
}

variable "service_annotations" {
  description = <<EOI
Map containing additional annotations to be added to the Cloud Run service
itself.
EOI
  type        = map(string)
  default     = {}
}

variable "ingress" {
  description = <<EOI
The ingress setting for the Cloud Run service. Possible values are
INGRESS_TRAFFIC_ALL, INGRESS_TRAFFIC_INTERNAL_ONLY, and
INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER.
EOI
  type        = string
  default     = null
}

variable "launch_stage" {
  description = <<EOI
The launch stage for the Cloud Run service. Possible values are UNIMPLEMENTED,
PRELAUNCH, EARLY_ACCESS, ALPHA, BETA, GA, and DEPRECATED.
EOI
  type        = string
  default     = "GA"
}

# Template block variables
variable "revision" {
  description = <<EOI
The unique name for the revision. If this field is omitted, it will be
automatically generated based on the Service name.
EOI
  type        = string
  default     = null
}

variable "template_labels" {
  description = <<EOI
A set of key/value label pairs to assign to the Cloud Run service revision.
EOI
  type        = map(string)
  default     = {}
}

variable "template_annotations" {
  description = <<EOI
Map containing additional annotations to be added to the Cloud Run service
template.
EOI
  type        = map(string)
  default     = {}
}

variable "timeout_seconds" {
  description = <<EOI
The maximum duration, in seconds, the instance is allowed for responding to a
request. Maximum is 900s.
EOI
  type        = string
  default     = "300s"
}

variable "execution_environment" {
  description = <<EOI
The sandbox environment to host this revision. Possible values are
EXECUTION_ENVIRONMENT_GEN1, and EXECUTION_ENVIRONMENT_GEN2.
EOI
  type        = string
  default     = "EXECUTION_ENVIRONMENT_GEN1"
}

variable "encryption_key" {
  description = <<EOI
The ID of a customer managed encryption key (CMEK) to use to encrypt this
container image.
EOI
  type        = string
  default     = null
}

variable "max_instance_request_concurrency" {
  description = <<EOI
Sets the maximum number of requests that each serving instance can receive.
EOI
  type        = number
  default     = null
}

variable "session_affinity" {
  description = <<EOI
Enables session affinity. For more information, go to
https://cloud.google.com/run/docs/configuring/session-affinity.
EOI
  type        = bool
  default     = null
}

variable "scaling" {
  description = <<EOI
The minimum number of auto-scaled instances defaults to 0, thus, the container
will stop if it doesn't receive requests for a period of time and the
following request will make the container start from cold. This should be
carefully considered for containers that take a significant amount of time
starting from cold.

For a container with N-workers, the maximum number of auto-scaled instances
should be less than 1/N of the maximum connection count for the Cloud SQL
instance.
EOI
  type = object({
    min_instance_count = optional(number)
    max_instance_count = optional(number)
  })
  default = null
}

variable "vpc_access" {
  description = <<EOI
Configure VPC access for the Cloud Run service. For more information on these
options see
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_service#nested_vpc_access
EOI
  type = object({
    connector = optional(string)
    egress    = optional(string)
    network_interfaces = optional(object({
      network    = optional(string)
      subnetwork = optional(string)
      tags       = optional(string)
    }))
  })
  default = null
}

variable "containers" {
  description = <<EOI
Configure one or more container instances for the service. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_service#nested_containers
for information on the available arguments.
EOI
  type = map(object({
    name    = optional(string)
    image   = string
    command = optional(list(string))
    args    = optional(list(string))
    env = optional(list(object({
      name  = string
      value = optional(string)
      value_source = optional(object({
        secret_key_ref = optional(object({
          secret  = string
          version = optional(string, "latest")
        }))
      }))
    })), [])
    resources = optional(object({
      limits            = optional(map(string))
      cpu_idle          = optional(bool, true)
      startup_cpu_boost = optional(bool, true)
    }))
    ports = optional(list(object({
      name           = optional(string)
      container_port = optional(number)
    })), [])
    volume_mounts = optional(list(object({
      name       = string
      mount_path = string
    })), [])
    working_dir = optional(string)
    liveness_probe = optional(object({
      initial_delay_seconds = optional(number)
      timeout_seconds       = optional(number)
      period_seconds        = optional(number)
      failure_threshold     = optional(number)
      http_get = optional(object({
        path = optional(string)
        port = optional(number)
        http_headers = optional(list(object({
          name  = string
          value = optional(string)
        })), [])
      }))
      grpc = optional(object({
        port    = optional(number)
        service = optional(string)
      }))
    }))
    startup_probe = optional(object({
      initial_delay_seconds = optional(number)
      timeout_seconds       = optional(number)
      period_seconds        = optional(number)
      failure_threshold     = optional(number)
      http_get = optional(object({
        path = optional(string)
        port = optional(number)
        http_headers = optional(list(object({
          name  = string
          value = optional(string)
        })), [])
      }))
      tcp_socket = optional(object({
        port = number
      }))
      grpc = optional(object({
        port    = optional(number)
        service = optional(string)
      }))
    }))
  }))
}

variable "volumes" {
  description = <<EOI
Configure one or more volumes for the service. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_service#nested_volumes
for more information on these options.
EOI
  type = list(object({
    name = string
    secret = optional(object({
      secret       = string
      default_mode = optional(number)
      items = optional(list(object({
        path    = string
        version = optional(string)
        mode    = optional(number)
      })), [])
    }))
    cloud_sql_instance = optional(object({
      instances = optional(list(string))
    }))
  }))
  default = []
}

variable "traffic" {
  description = <<EOI
Configure traffic allocation between one or more service revisions.
EOI
  type = list(object({
    type     = optional(string)
    revision = optional(string)
    percent  = optional(number)
    tag      = optional(string)
  }))
  default = []
}

variable "mount_cloudsql_instance" {
  description = <<EOI
Mount a single CloudSQL instance in each container for the service. This value
should be the Cloud SQL instance connection name, for example
"example-devel-e662dd2b:europe-west2:sql-6e1dd60b". This is a convenience
variable to simplify mounting a single Cloud SQL instance. If you require more
control you can define one or more Cloud SQL mounts directly using
var.containers and var.volumes instead.
EOI
  type        = string
  default     = null
}

variable "sql_instance_project" {
  description = "Project containing SQL instance. Defaults to var.project."
  type        = string
  default     = null
}

variable "grant_sql_client_role_to_webapp_sa" {
  description = <<EOI
When set to true the roles/cloudsql.client role will be granted to the webapp
service account at the project level to allow it to connect to Cloud SQL.
EOI
  type        = bool
  default     = false
}

variable "allow_unauthenticated_invocations" {
  description = <<EOI
If true, the webapp will allow unauthenticated invocations. If false, the
webapp requires authentication as a Google user with the Cloud Run invoker
permission on the deployment.
EOI
  type        = bool
  default     = true
}

# Load Balancer variables
variable "enable_load_balancer" {
  type        = bool
  default     = false
  description = <<EOI
Whether to configure a load balancer or use the default run.app generated
hostname.
EOI
}

variable "dns_names" {
  type        = map(string)
  default     = {}
  description = <<EOI
DNS names to configure for the web application. Note that DNS records are
_NOT_ created, they are used in the load balancer module to ensure the SSL
certificate is generated with the required SANs. The map's keys are arbitrary
and are only required to avoid errors when the DNS name is a value which
Terraform does not know until after the apply operation.

For example:

{ my_awesome_dns_name = "awesome.example.com" }
EOI
}

variable "ssl_certificates" {
  type        = list(any)
  default     = []
  description = <<EOI
A list of self-links to any custom TLS certificates to add to the load
balancer. Requires that var.use_load_balancer be "true". The self-link is
available as the "self_link" attribute of "google_compute_ssl_certificate"
resources.
EOI
}

variable "ssl_policy" {
  type    = string
  default = null

  description = <<EOI
By default, the google_compute_ssl_policy.default SSL policy is applied to the
load balancer in load_balancer.tf. This sets the SSL profile to MODERN and
restricts TLS to >= 1.2. If a different SSL policy is required, it should be
created outside of this module and its ID passed through using this variable.
EOI
}

variable "enable_ipv6" {
  type        = bool
  default     = false
  description = "Whether to enable IPv6 address on the CDN load-balancer."
}

variable "create_ipv6_address" {
  type        = bool
  default     = false
  description = <<EOI
Allocate an IPv6 address to the load balancer if var.enable_ipv6 is true.
EOI
}

# Cloud Run pre-deploy job variables
variable "enable_pre_deploy_job" {
  description = <<EOI
Configure a Cloud Run Job to be executed *before* the main Cloud Run service
is deployed. This is useful for running database migrations for example.
EOI
  type        = bool
  default     = false
}

variable "pre_deploy_job_trigger" {
  description = <<EOI
When true, the pre-deploy Cloud Run job is executed via a
null_resource-triggered gcloud command whenever Terraform detects that
var.pre_deploy_job_container.image has changed.
EOI
  type        = bool
  default     = true
}

variable "pre_deploy_job_force" {
  description = <<EOI
When true, and only when used in addition to var.pre_deploy_job_trigger, the
pre-deploy Cloud Run job is executed at every terraform apply, regardless of
# the status of var.pre_deploy_job_container.image.
EOI
  type        = bool
  default     = false
}

variable "pre_deploy_job_labels" {
  description = <<EOI
Map of key/value pairs containing labels to assign to the pre-deploy Cloud Run
job.
EOI
  type        = map(string)
  default     = null
}

variable "pre_deploy_job_annotations" {
  description = <<EOI
Map of key/value pairs containing annotations to assign to the pre-deploy Cloud
Run job.
EOI
  type        = map(string)
  default     = null
}

variable "pre_deploy_job_parallelism" {
  description = <<EOI
Specifies the maximum desired number of tasks the execution should run at
given time.
EOI
  type        = number
  default     = null
}

variable "pre_deploy_job_task_count" {
  description = "Specifies the desired number of tasks the execution should run."
  type        = number
  default     = null
}

variable "pre_deploy_job_launch_stage" {
  description = <<EOI
The launch stage for the pre-deploy Cloud Run job. Possible values are UNIMPLEMENTED,
PRELAUNCH, EARLY_ACCESS, ALPHA, BETA, GA, and DEPRECATED.
EOI
  type        = string
  default     = "GA"
}

variable "pre_deploy_job_container" {
  description = <<EOI
Configure the container instance for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_containers
for more information on these options.
EOI
  type = object({
    name    = optional(string)
    image   = optional(string)
    command = optional(list(string))
    args    = optional(list(string))
    env = optional(list(object({
      name  = string
      value = optional(string)
      value_source = optional(object({
        secret_key_ref = optional(object({
          secret  = string
          version = optional(string, "latest")
        }))
      }))
    })), [])
    resources = optional(object({
      limits = optional(map(string))
    }))
    ports = optional(list(object({
      name           = optional(string)
      container_port = optional(number)
    })), [])
    volume_mounts = optional(list(object({
      name       = string
      mount_path = string
    })), [])
    working_dir = optional(string)
  })
  default = null
}

variable "pre_deploy_job_volumes" {
  description = <<EOI
Configure one or more volumes for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_volumes
for more information on these options.
EOI
  type = list(object({
    name = string
    secret = optional(object({
      secret       = string
      default_mode = optional(number)
      items = optional(list(object({
        path    = string
        version = optional(string)
        mode    = optional(number)
      })), [])
    }))
    cloud_sql_instance = optional(object({
      instances = optional(list(string))
    }))
  }))
  default = []
}

variable "pre_deploy_job_timeout" {
  description = "Configure a timeout, in seconds, for the pre-deploy job."
  type        = string
  default     = null
}

variable "pre_deploy_job_execution_environment" {
  description = <<EOI
The execution environment to host this task. Possible values are
EXECUTION_ENVIRONMENT_GEN1, and EXECUTION_ENVIRONMENT_GEN2
EOI
  type        = string
  default     = "EXECUTION_ENVIRONMENT_GEN2"
}

variable "pre_deploy_job_encryption_key" {
  description = <<EOI
The ID of a customer managed encryption key (CMEK) to use to encrypt this
container image.
EOI
  type        = string
  default     = null
}

variable "pre_deploy_job_vpc_access" {
  description = <<EOI
Configure VPC access for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_vpc_access
for more information on these options.
EOI
  type = object({
    connector = optional(string)
    egress    = optional(string)
    network_interfaces = optional(object({
      network    = optional(string)
      subnetwork = optional(string)
      tags       = optional(string)
    }))
  })
  default = null
}

variable "pre_deploy_job_max_retries" {
  description = "Configure the maximum number of retries for the pre-deploy job."
  type        = number
  default     = null
}

variable "pre_deploy_job_mount_cloudsql_instance" {
  description = <<EOI
Mount a CloudSQL instance in the pre-deploy job container. This is a
convenience variable to simplify mounting a Cloud SQL instance. However, if
you require more control over this you should define it directly in
var.pre_deploy_job_container instead.
EOI
  type        = string
  default     = null
}

# Monitoring and alerting
variable "monitoring_scoping_project" {
  description = <<EOI
The ID of a Cloud Monitoring scoping project to create monitoring resources
in. If omitted, var.project will be used instead.
EOI
  type        = string
  default     = null
}

variable "enable_alerting" {
  type        = bool
  default     = true
  description = "Enable alerting policies."
}

variable "alerting_notification_channels" {
  default     = []
  type        = list(string)
  description = <<EOI
A list of notification channel IDs to send uptime alerts to. The format for
the channel IDs should be
"projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]".
EOI
}

variable "alerting_uptime_timeout" {
  default     = "30s"
  type        = string
  description = "Timeout for http polling."
}

variable "alerting_uptime_period" {
  type        = string
  default     = "300s"
  description = "Frequency of uptime checks"
}

variable "alerting_success_threshold_percent" {
  type        = number
  default     = 75
  description = <<EOI
If the percentage of successful uptime checks within the given uptime period
falls below this, an alert will be triggered. Set to 100 to trigger an alert
if any uptime check fails, set to a lower number to tolerate failures without
alerting.

Experience has taught us that uptime checks can fail semi-regularly due to
transient problems outside our control, therefore we allow some leeway before
triggering an alert.
EOI
}

variable "enable_monitoring" {
  default     = false
  type        = bool
  description = <<EOI
Optional. If true, create uptime and SSL expiry checks.

Note that this is different from not specifying an alerting email address. If
no alerting email address is specified the uptime checks are still created,
they just don't alert if they fail.
EOI
}

variable "monitoring_path" {
  type        = string
  default     = "/"
  description = "Path component of url to be monitored."
}

# Service account variables
variable "service_account_id" {
  default     = ""
  type        = string
  description = <<EOI
A service account is always created for the web application. If non-empty this
variable overrides the default service account id. The default id is formed
from the "name" variable value with "-run" appended.
EOI
}

variable "service_account_display_name" {
  default     = ""
  type        = string
  description = <<EOI
If non-empty, override the default display name of the webapp service account.
EOI
}

# Static egress variables
variable "enable_static_egress_ip" {
  default     = false
  type        = bool
  description = <<EOI
Whether to assign a static ip for egress from this cloud run instance. If
enabled, the "vpcaccess.googleapis.com" API must also be enabled on the
project.
EOI
}

variable "static_egress_ip_cidr_range" {
  default     = "10.124.0.0/28"
  type        = string
  description = <<EOI
The cidr range used to create a subnet that this cloud run will use if assigned
a static ip
EOI
}

# tflint-ignore: terraform_unused_declarations
variable "static_egress_ip_subnetwork_id" {
  description = <<EOI
When using an existing VPC Access Connector with the static egress IP
configuration an existing subnetwork must be provided.
EOI
  type        = string
  default     = null
}

variable "min_ports_per_vm" {
  default     = 64
  type        = number
  description = <<EOI
When using Cloud NAT to provide an egress route, Cloud NAT's minimum ports per
VM can be configured to determine how many concurrent connections can be
established to the same destination IP address and port.
EOI
}

variable "vpc_access_connector_max_throughput" {
  type        = number
  default     = 300
  description = <<EOI
Optional. The maximum throughput of the connector in megabytes per second.
Defaults to 300.
EOI
}

variable "vpc_access_connector_min_throughput" {
  type        = number
  default     = 200
  description = <<EOI
Optional. The minimum throughput of the connector in megabytes per second.
Defaults to 200.
EOI
}

variable "load_balancer_backend" {
  type = object({
    port_name               = optional(string)
    description             = optional(string)
    enable_cdn              = optional(bool, false)
    compression_mode        = optional(string)
    security_policy         = optional(string, null)
    edge_security_policy    = optional(string, null)
    custom_request_headers  = optional(list(string))
    custom_response_headers = optional(list(string))

    connection_draining_timeout_sec = optional(number)
    session_affinity                = optional(string)
    affinity_cookie_ttl_sec         = optional(number)
    locality_lb_policy              = optional(string)

    log_config = optional(object({
      enable      = optional(bool)
      sample_rate = optional(number)
    }))

    cdn_policy = optional(object({
      cache_mode                   = optional(string)
      signed_url_cache_max_age_sec = optional(string)
      default_ttl                  = optional(number)
      max_ttl                      = optional(number)
      client_ttl                   = optional(number)
      negative_caching             = optional(bool)
      negative_caching_policy = optional(object({
        code = optional(number)
        ttl  = optional(number)
      }))
      serve_while_stale = optional(number)
      cache_key_policy = optional(object({
        include_host           = optional(bool)
        include_protocol       = optional(bool)
        include_query_string   = optional(bool)
        query_string_blacklist = optional(list(string))
        query_string_whitelist = optional(list(string))
        include_http_headers   = optional(list(string))
        include_named_cookies  = optional(list(string))
      }))
      bypass_cache_on_request_headers = optional(list(string))
    }))

    outlier_detection = optional(object({
      base_ejection_time = optional(object({
        seconds = number
        nanos   = optional(number)
      }))
      consecutive_errors                    = optional(number)
      consecutive_gateway_failure           = optional(number)
      enforcing_consecutive_errors          = optional(number)
      enforcing_consecutive_gateway_failure = optional(number)
      enforcing_success_rate                = optional(number)
      interval = optional(object({
        seconds = number
        nanos   = optional(number)
      }))
      max_ejection_percent        = optional(number)
      success_rate_minimum_hosts  = optional(number)
      success_rate_request_volume = optional(number)
      success_rate_stdev_factor   = optional(number)
    }))
  })
  default     = {}
  description = <<EOI
Optional. Custom options for the backend load balancer.
Defaults to {}.
EOI
}

variable "deletion_protection" {
  default     = true
  type        = bool
  description = <<EOI
Optional. Whether Terraform will be prevented from destroying
the Cloud Run resource. Defaults to true.
EOI
}

variable "local_files_dir" {
  description = <<-EOI
    A local directory where files may be created which persist between CI
    pipeline runs but which are not checked into source control.
    This variable is only to be used to pass a directory to the
    gcp-site-monitoring module call.
  EOI
  type        = string
  default     = null
}

variable "alerting_load_balancer_server_errors" {
  type = object({
    enabled            = optional(bool, true)
    alignment_period   = optional(string, "60s")
    threshold          = optional(number, 10)
    policy_name        = optional(string)
    policy_name_prefix = optional(string)
    condition_name     = optional(string)
  })
  default     = {}
  description = "Optional. Load balancer server error alerting configuration."
}
