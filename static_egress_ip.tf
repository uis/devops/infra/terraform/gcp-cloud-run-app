# Create a subnet, vpc access connector, router,  and nat to allow a cloud run instance to be assigned a static ip. The
# VPC connector is assigned to the cloud run's annotations. This follows the steps detailed here:
# https://cloud.google.com/run/docs/configuring/static-outbound-ip

# trivy:ignore:AVD-GCP-0029
# kics-scan disable=40430747-442d-450a-a34f-dc57149f4609
resource "google_compute_subnetwork" "vpc_connector" {
  count = local.create_vpc_connector ? 1 : 0

  name                     = "${var.name}-vpc-connector"
  project                  = var.project
  ip_cidr_range            = var.static_egress_ip_cidr_range
  network                  = "default"
  region                   = var.region
  private_ip_google_access = true
}

resource "google_vpc_access_connector" "main" {
  count = local.create_vpc_connector ? 1 : 0

  name           = "${var.name}-conn"
  project        = var.project
  region         = var.region
  max_throughput = var.vpc_access_connector_max_throughput
  min_throughput = var.vpc_access_connector_min_throughput

  subnet {
    name = google_compute_subnetwork.vpc_connector[0].name
  }
}

resource "google_compute_router" "static_ip" {
  count = var.enable_static_egress_ip ? 1 : 0

  name    = "${var.name}-ip-router"
  project = var.project
  network = "default"
  region  = var.region
}

resource "google_compute_address" "static_ip" {
  count = var.enable_static_egress_ip ? 1 : 0

  name    = "${var.name}-static-ip"
  project = var.project
  region  = var.region

  # We do not expect that static IPs should be regularly removed and recreated,
  # as it's likely that they will be used within firewall configuration outside
  # of our GCP deployment. So we make them difficult to destroy, guarding against
  # an accidentally removal.
  lifecycle {
    prevent_destroy = true
  }
}

resource "google_compute_router_nat" "static_ip" {
  count = var.enable_static_egress_ip ? 1 : 0

  name                               = "${var.name}-static-ip-nat"
  project                            = var.project
  router                             = google_compute_router.static_ip[0].name
  nat_ips                            = [google_compute_address.static_ip[0].self_link]
  nat_ip_allocate_option             = "MANUAL_ONLY"
  region                             = var.region
  min_ports_per_vm                   = var.min_ports_per_vm
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name = (
      local.create_vpc_connector ? google_compute_subnetwork.vpc_connector[0].id : var.static_egress_ip_subnetwork_id
    )
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

moved {
  from = google_compute_subnetwork.isolated-subnet
  to   = google_compute_subnetwork.vpc_connector
}
moved {
  from = google_vpc_access_connector.static-ip-connector
  to   = google_vpc_access_connector.main
}
moved {
  from = google_compute_router.static-ip-router
  to   = google_compute_router.static_ip
}
moved {
  from = google_compute_address.static-ip
  to   = google_compute_address.static_ip
}
moved {
  from = google_compute_router_nat.static-ip-nat
  to   = google_compute_router_nat.static_ip
}
