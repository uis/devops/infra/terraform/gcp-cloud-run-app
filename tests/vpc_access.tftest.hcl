run "setup" {
  variables {
    create_vpc_connector = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_service_vpc_access" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    vpc_access = {
      connector = run.setup.vpc_connector_id
      egress    = "PRIVATE_RANGES_ONLY"
    }
    deletion_protection = false
  }

  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].vpc_access) == 1
    error_message = "The Cloud Run service should be configured with a single VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].vpc_access[0].connector == run.setup.vpc_connector_id
    error_message = "The Cloud Run service should be configured to use the run.setup.vpc_connector_id VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].vpc_access[0].egress == "PRIVATE_RANGES_ONLY"
    error_message = "The Cloud Run service should be configured to route private ranges only to the VPC Access Connector."
  }
}

run "test_pre_deploy_job_vpc_access" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_pre_deploy_job = true
    pre_deploy_job_container = {
      image   = "golang:latest"
      command = ["go"]
      args    = ["version"]
    }
    pre_deploy_job_vpc_access = {
      connector = run.setup.vpc_connector_id
      egress    = "PRIVATE_RANGES_ONLY"
    }
    deletion_protection = false
  }

  assert {
    condition     = length(google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].vpc_access) == 1
    error_message = "The Cloud Run service should be configured with a single VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].vpc_access[0].connector == run.setup.vpc_connector_id
    error_message = "The Cloud Run service should be configured to use the run.setup.vpc_connector_id VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].vpc_access[0].egress == "PRIVATE_RANGES_ONLY"
    error_message = "The Cloud Run service should be configured to route private ranges only to the VPC Access Connector."
  }
}

run "test_static_egress_ip_config_with_existing_access_controller" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    vpc_access = {
      connector = run.setup.vpc_connector_id
    }
    enable_static_egress_ip        = true
    static_egress_ip_subnetwork_id = run.setup.subnetwork_id
    deletion_protection            = false
  }

  assert {
    condition     = length(google_compute_router.static_ip) == 1
    error_message = "A single compute router resource should be created."
  }
  assert {
    condition     = length(google_compute_address.static_ip) == 1
    error_message = "A single compute address resource should be created."
  }
  assert {
    condition     = length(google_compute_router_nat.static_ip) == 1
    error_message = "A single compute router NAT resource should be created."
  }
  assert {
    condition     = google_compute_router_nat.static_ip[0].router == google_compute_router.static_ip[0].name
    error_message = "The NAT resource should be configured to use the google_compute_router.static_ip[0] router resource."
  }
  assert {
    condition     = length(google_compute_router_nat.static_ip[0].subnetwork) == 1
    error_message = "The NAT resource should be configured with a single subnetwork."
  }
  assert {
    condition     = [for subnet in google_compute_router_nat.static_ip[0].subnetwork : subnet.name][0] == run.setup.subnetwork_id
    error_message = "The NAT resource should be configured to use the run.setup.subnetwork_id subnetwork resource."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].vpc_access) == 1
    error_message = "The Cloud Run service should be configured with a single VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].vpc_access[0].connector == run.setup.vpc_connector_id
    error_message = "The Cloud Run service should be configured to use the run.setup.vpc_connector_id VPC Access Connector."
  }
}

run "test_static_egress_ip_config_with_defaults" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_static_egress_ip = true
    deletion_protection     = false
  }

  assert {
    condition     = length(google_compute_subnetwork.vpc_connector) == 1
    error_message = "A single subnetwork should be created for the VPC Access Connector to utilise."
  }
  assert {
    condition     = length(google_vpc_access_connector.main) == 1
    error_message = "A single VPC Access Connector resource should be created."
  }
  assert {
    condition     = google_vpc_access_connector.main[0].subnet[0].name == google_compute_subnetwork.vpc_connector[0].name
    error_message = "The VPC Access Connector should be configured to use the google_compute_subnetwork.vpc_connector[0] subnetwork."
  }
  assert {
    condition     = length(google_compute_router.static_ip) == 1
    error_message = "A single compute router resource should be created."
  }
  assert {
    condition     = length(google_compute_address.static_ip) == 1
    error_message = "A single compute address resource should be created."
  }
  assert {
    condition     = length(google_compute_router_nat.static_ip) == 1
    error_message = "A single compute router NAT resource should be created."
  }
  assert {
    condition     = google_compute_router_nat.static_ip[0].router == google_compute_router.static_ip[0].name
    error_message = "The NAT resource should be configured to use the google_compute_router.static_ip[0] router resource."
  }
  assert {
    condition     = length(google_compute_router_nat.static_ip[0].subnetwork) == 1
    error_message = "The NAT resource should be configured with a single subnetwork."
  }
  assert {
    condition     = [for subnet in google_compute_router_nat.static_ip[0].subnetwork : subnet.name][0] == google_compute_subnetwork.vpc_connector[0].id
    error_message = "The NAT resource should be configured to use the google_compute_subnetwork.vpc_connector[0] subnetwork resource."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].vpc_access) == 1
    error_message = "The Cloud Run service should be configured with a single VPC Access Connector."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].vpc_access[0].connector == google_vpc_access_connector.main[0].id
    error_message = "The Cloud Run service should be configured to use the google_vpc_access_connector.main[0].id resource."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].vpc_access[0].egress == "ALL_TRAFFIC"
    error_message = "The Cloud Run service should be configured to route all egress traffic to the VPC Access Connector."
  }
}
