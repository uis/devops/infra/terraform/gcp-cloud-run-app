run "setup" {
  variables {
    create_test_sql_instances = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_cloudsql_mount_using_helper_variable" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    mount_cloudsql_instance = run.setup.instance_connection_names[0]
    deletion_protection     = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].name == "cloudsql"
    error_message = "A single volume should be configured named 'cloudsql'."
  }
  assert {
    condition = [
      for instance in google_cloud_run_v2_service.webapp.template[0].volumes[0].cloud_sql_instance[0].instances : instance
    ][0] == run.setup.instance_connection_names[0]
    error_message = "The cloudsql volume should be configured to use the run.setup.instance_connection_names[0] instance connection name."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].name == "cloudsql"
    error_message = "A single volume mount should be configured with the name 'cloudsql'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].mount_path == "/cloudsql"
    error_message = "A single volume mount should be configured with the mount path '/cloudsql'."
  }
}

run "test_cloudsql_mount_to_single_container" {
  variables {
    name = run.setup.random_name
    volumes = [
      {
        name = "cloudsql"
        cloud_sql_instance = {
          instances = [run.setup.instance_connection_names[0]]
        }
      }
    ]
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        volume_mounts = [
          {
            name       = "cloudsql"
            mount_path = "/cloudsql"
          }
        ]
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].name == "cloudsql"
    error_message = "A single volume should be configured named 'cloudsql'."
  }
  assert {
    condition = (
      [for instance in google_cloud_run_v2_service.webapp.template[0].volumes[0].cloud_sql_instance[0].instances : instance][0]
      == run.setup.instance_connection_names[0]
    )
    error_message = "The cloudsql volume should be configured to use the run.setup.instance_connection_names[0] instance connection name."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].name == "cloudsql"
    error_message = "A single volume mount should be configured with the name 'cloudsql'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].mount_path == "/cloudsql"
    error_message = "A single volume mount should be configured with the mount path '/cloudsql'."
  }
}

run "test_cloudsql_mount_multiple_cloudsql_instances" {
  variables {
    name = run.setup.random_name
    volumes = [
      {
        name = "cloudsql"
        cloud_sql_instance = {
          instances = run.setup.instance_connection_names
        }
      }
    ]
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        volume_mounts = [
          {
            name       = "cloudsql"
            mount_path = "/cloudsql"
          }
        ]
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].name == "cloudsql"
    error_message = "A single volume should be configured named 'cloudsql'."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].volumes[0].cloud_sql_instance[0].instances) == 2
    error_message = "The cloudsql volume should be configured with exactly 2 instances."
  }
  assert {
    condition = (
      contains(
        google_cloud_run_v2_service.webapp.template[0].volumes[0].cloud_sql_instance[0].instances,
        run.setup.instance_connection_names[0]
      )
      && contains(
        google_cloud_run_v2_service.webapp.template[0].volumes[0].cloud_sql_instance[0].instances,
        run.setup.instance_connection_names[1]
      )
    )
    error_message = "The cloudsql volume should be configured with the two instance connection names defined in var.volumes."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].name == "cloudsql"
    error_message = "A single volume mount should be configured with the name 'cloudsql'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].mount_path == "/cloudsql"
    error_message = "A single volume mount should be configured with the mount path '/cloudsql'."
  }
}

run "test_cloudsql_mount_to_pre_deploy_job_using_helper_variable" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_pre_deploy_job = true
    pre_deploy_job_container = {
      image   = "golang:latest"
      command = ["go"]
      args    = ["version"]
    }
    pre_deploy_job_mount_cloudsql_instance = run.setup.instance_connection_names[0]
    deletion_protection                    = false
  }

  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].name == "cloudsql"
    error_message = "A single volume should be configured named 'cloudsql'."
  }
  assert {
    condition = (
      [for instance in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].cloud_sql_instance[0].instances : instance][0]
      == run.setup.instance_connection_names[0]
    )
    error_message = "The cloudsql volume should be configured to use the run.setup.instance_connection_names[0] instance connection name."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].name == "cloudsql"
    error_message = "A single volume mount should be configured with the name 'cloudsql'."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].mount_path == "/cloudsql"
    error_message = "A single volume mount should be configured with the mount path '/cloudsql'."
  }
}

run "test_cloudsql_mount_to_pre_deploy_job" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_pre_deploy_job = true
    pre_deploy_job_volumes = [
      {
        name = "cloudsql"
        cloud_sql_instance = {
          instances = [run.setup.instance_connection_names[0]]
        }
      }
    ]
    pre_deploy_job_container = {
      image   = "golang:latest"
      command = ["go"]
      args    = ["version"]
      volume_mounts = [
        {
          name       = "cloudsql"
          mount_path = "/cloudsql"
        }
      ]
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].name == "cloudsql"
    error_message = "A single volume should be configured named 'cloudsql'."
  }
  assert {
    condition = (
      [for instance in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].cloud_sql_instance[0].instances : instance][0]
      == run.setup.instance_connection_names[0]
    )
    error_message = "The cloudsql volume should be configured to use the run.setup.instance_connection_names[0] instance connection name."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].name == "cloudsql"
    error_message = "A single volume mount should be configured with the name 'cloudsql'."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].mount_path == "/cloudsql"
    error_message = "A single volume mount should be configured with the mount path '/cloudsql'."
  }
}
