#! /usr/bin/env bash

set -e

current_verbosity=$(gcloud config get core/verbosity)
gcloud config set core/verbosity error

cleanup() {
    gcloud config unset auth/impersonate_service_account
    gcloud config set core/verbosity "$current_verbosity"
}

trap 'cleanup' EXIT INT TERM

TEST_PREFIX="test-rapp"
# Workaround to detect cloud functions created by GCP Minimal Site Monitoring module.
# The cloud functions created by this module use `short_service_name`, (first 8 symbols).
# https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring/-/blob/master/locals.tf?ref_type=heads#L74
# SHORT_TEST_PREFIX is used later in this script in "Cleaning up Cloud Functions..." block.
SHORT_TEST_PREFIX="${TEST_PREFIX:0:8}"
GCP_PROJECT="infra-testing-int-e2395220"
GCP_PROJECT_META="infra-testing-meta-21f09a44"
GCP_REGION="europe-west2"
GCP_SERVICE_ACCOUNT="terraform-deploy@infra-testing-int-e2395220.iam.gserviceaccount.com"

gcloud config set auth/impersonate_service_account $GCP_SERVICE_ACCOUNT

echo "Cleaning up Cloud Run services..."
mapfile -t services < <(
    gcloud --project="$GCP_PROJECT" run services --region="$GCP_REGION" list \
        --filter="metadata.name ~ ${TEST_PREFIX}.*" --format="value(metadata.name)"
)

for service in "${services[@]}"; do
    echo "Removing Cloud Run service '${service}'"
    gcloud --project="$GCP_PROJECT" run services --region="$GCP_REGION" delete "$service" --quiet
done

echo "Cleaning up IAM service accounts..."
mapfile -t service_accounts < <(
    gcloud --project="$GCP_PROJECT" iam service-accounts list \
        --filter="email ~ ${TEST_PREFIX}[0-9a-fA-F]+?-run|${TEST_PREFIX}[0-9a-fA-F]+?-uptime" \
        --format="value(email)"
)

for account in "${service_accounts[@]}"; do
    gcloud --project="$GCP_PROJECT" iam service-accounts delete "$account" --quiet
done

echo "Cleaning up Cloud Functions..."
mapfile -t functions < <(
    gcloud --project="$GCP_PROJECT" functions list \
        --filter="name ~ .*${SHORT_TEST_PREFIX}.*" --format="value(name)"
)

for function in "${functions[@]}"; do
    gcloud --project="$GCP_PROJECT" functions delete --region="$GCP_REGION" "$function" --quiet
done

echo "Cleaning up Cloud Storage buckets..."
mapfile -t buckets < <(
    gcloud --project="$GCP_PROJECT" storage buckets list \
        --filter="name ~ ${TEST_PREFIX::8}-uptime" --format="value(storage_url)"
)

for bucket in "${buckets[@]}"; do
    gcloud --project="$GCP_PROJECT" storage rm -r "$bucket" --quiet
done

echo "Cleaning up Cloud Monitoring resources..."
mapfile -t alert_policies < <(
    gcloud alpha --project="$GCP_PROJECT" monitoring policies list \
        --filter="displayName ~ Uptime\scheck\sfor\s${TEST_PREFIX}[0-9a-fA-F]+?-.*|SSL\sexpiry\scheck\sfor\s${TEST_PREFIX}[0-9a-fA-F]+?-.*" \
        --format="value(name)"
)

for policy in "${alert_policies[@]}"; do
    gcloud alpha monitoring policies delete "$policy" --quiet
done

mapfile -t alert_policies_meta < <(
    gcloud alpha --project="$GCP_PROJECT_META" monitoring policies list \
        --filter="displayName ~ Uptime\scheck\sfor\s${TEST_PREFIX}[0-9a-fA-F]+?-.*|SSL\sexpiry\scheck\sfor\s${TEST_PREFIX}[0-9a-fA-F]+?-.*" \
        --format="value(name)"
)

for policy_meta in "${alert_policies_meta[@]}"; do
    gcloud alpha monitoring policies delete "$policy_meta" --quiet
done

mapfile -t uptime_checks < <(
    gcloud --project="$GCP_PROJECT" monitoring uptime list-configs \
        --filter="displayName ~ ${TEST_PREFIX}[0-9a-fA-F]+?-.*" --format="value(name)"
)

for check in "${uptime_checks[@]}"; do
    gcloud monitoring uptime delete "$check" --quiet
done

echo "Cleaning up Cloud Run jobs..."
mapfile -t jobs < <(
    gcloud --project="$GCP_PROJECT" run jobs list \
        --filter="metadata.name ~ ${TEST_PREFIX}.*" --format="value(metadata.name)"
)

for job in "${jobs[@]}"; do
    gcloud --project="$GCP_PROJECT" run jobs --region="$GCP_REGION" delete "$job" --quiet
done

echo "Cleaning up load balancer resources..."
mapfile -t http_proxies < <(
    gcloud --project="$GCP_PROJECT" compute target-http-proxies list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for http_proxy in "${http_proxies[@]}"; do
    gcloud --project="$GCP_PROJECT" compute target-http-proxies delete "$http_proxy" --global --quiet
done

mapfile -t https_proxies < <(
    gcloud --project="$GCP_PROJECT" compute target-https-proxies list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for https_proxy in "${https_proxies[@]}"; do
    gcloud --project="$GCP_PROJECT" compute target-https-proxies delete "$https_proxy" --global --quiet
done

mapfile -t ssl_certs < <(
    gcloud --project="$GCP_PROJECT" compute ssl-certificates list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for cert in "${ssl_certs[@]}"; do
    gcloud --project="$GCP_PROJECT" compute ssl-certificates delete "$cert" --global --quiet
done

mapfile -t url_maps < <(
    gcloud --project="$GCP_PROJECT" compute url-maps list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for url_map in "${url_maps[@]}"; do
    gcloud --project="$GCP_PROJECT" compute url-maps delete "$url_map" --global --quiet
done

mapfile -t backend_services < <(
    gcloud --project="$GCP_PROJECT" compute backend-services list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for service in "${backend_services[@]}"; do
    gcloud --project="$GCP_PROJECT" compute backend-services delete "$service" --global --quiet
done

mapfile -t serverless_negs < <(
    gcloud --project="$GCP_PROJECT" compute network-endpoint-groups list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for neg in "${serverless_negs[@]}"; do
    gcloud --project="$GCP_PROJECT" compute network-endpoint-groups delete "$neg" --region="$GCP_REGION" --quiet
done

mapfile -t ssl_policies < <(
    gcloud --project="$GCP_PROJECT" compute ssl-policies list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for ssl_policy in "${ssl_policies[@]}"; do
    gcloud --project="$GCP_PROJECT" compute ssl-policies delete "$ssl_policy" --global --quiet
done

echo "Cleaning up network resources..."
mapfile -t connectors < <(
    gcloud --project="$GCP_PROJECT" compute networks vpc-access connectors list --region="$GCP_REGION" \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for conn in "${connectors[@]}"; do
    gcloud --project="$GCP_PROJECT" compute networks vpc-access connectors delete "$conn" \
        --region="$GCP_REGION" --quiet
done

mapfile -t routers < <(
    gcloud --project="$GCP_PROJECT" compute routers list --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for router in "${routers[@]}"; do
    gcloud --project="$GCP_PROJECT" compute routers delete "$router" --region="$GCP_REGION" --quiet
done

mapfile -t addresses < <(
    gcloud --project="$GCP_PROJECT" compute addresses list --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)" \
        --global
)

for address in "${addresses[@]}"; do
    gcloud --project="$GCP_PROJECT" compute addresses delete "$address" --global --quiet
done

mapfile -t subnets < <(
    gcloud --project="$GCP_PROJECT" compute networks subnets list \
        --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for subnet in "${subnets[@]}"; do
    gcloud --project="$GCP_PROJECT" compute networks subnets delete "$subnet" --region="$GCP_REGION" --quiet
done

echo "Cleaning up test setup resources..."
mapfile -t instances < <(
    gcloud --project="$GCP_PROJECT" sql instances list --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for instance in "${instances[@]}"; do
    gcloud --project="$GCP_PROJECT" sql instances delete "$instance" --quiet
done

mapfile -t secrets < <(
    gcloud --project="$GCP_PROJECT" secrets list --filter="name ~ ${TEST_PREFIX}.*" --format="value(name)"
)

for secret in "${secrets[@]}"; do
    gcloud --project="$GCP_PROJECT" secrets delete "$secret" --quiet
done
