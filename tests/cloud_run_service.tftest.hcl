run "setup" {
  variables {
    create_test_secrets = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_service_with_default_variable_values" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.ingress == "INGRESS_TRAFFIC_ALL"
    error_message = "Ingress should be 'INGRESS_TRAFFIC_ALL'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.launch_stage == "GA"
    error_message = "Launch stage should be 'GA'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.location == "europe-west2"
    error_message = "Location should be 'europe-west2'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].execution_environment == "EXECUTION_ENVIRONMENT_GEN1"
    error_message = "Execution environment should be 'EXECUTION_ENVIRONMENT_GEN1'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].timeout == "300s"
    error_message = "Timeout should be '300s'."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].containers) == 1
    error_message = "A single container block should be defined."
  }
  assert {
    condition     = google_cloud_run_v2_service_iam_member.webapp_all_users_invoker[0].role == "roles/run.invoker"
    error_message = "google_cloud_run_v2_service_iam_member.webapp_all_users_invoker.role should be 'roles/run.invoker'."
  }
  assert {
    condition     = google_cloud_run_v2_service_iam_member.webapp_all_users_invoker[0].member == "allUsers"
    error_message = "google_cloud_run_v2_service_iam_member.webapp_all_users_invoker.member should be 'allUsers'."
  }
  assert {
    condition     = google_service_account.webapp.name != null
    error_message = "A dedicated service account should be created for the Cloud Run service."
  }
}

run "test_container_resources_block" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        resources = {
          startup_cpu_boost = false
          cpu_idle          = false
          limits = {
            cpu = 2
          }
        }
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].startup_cpu_boost == false
    error_message = "startup_cpu_boost should be 'false'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].cpu_idle == false
    error_message = "cpu_idle should be 'false'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].limits.cpu == "2"
    error_message = "limits.cpu should be '2'."
  }
}

run "test_container_resources_block_defaults" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        resources = {
          startup_cpu_boost = true
          cpu_idle          = true
          limits = {
            cpu = 2
          }
        }
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].startup_cpu_boost == true
    error_message = "startup_cpu_boost should be 'true'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].cpu_idle == true
    error_message = "cpu_idle should be 'true'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].resources[0].limits.cpu == "2"
    error_message = "limits.cpu should be '2'."
  }
}

run "setup_secret_iam" {
  variables {
    create_test_secrets     = true
    create_test_secrets_iam = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_env_vars_and_secrets" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        env = [
          {
            name  = "HELLO",
            value = "WORLD!"
          },
          {
            name = "SECRET",
            value_source = {
              secret_key_ref = {
                secret = run.setup.secret_ids[0]
              }
            }
          }
        ]
        volume_mounts = [
          {
            name       = "secret-volume",
            mount_path = "/secrets"
          }
        ]
      }
    }
    volumes = [
      {
        name = "secret-volume",
        secret = {
          secret = run.setup.secret_ids[1]
          items = [
            {
              version = "1",
              path    = "my-secret"
            }
          ]
        }
      }
    ]
    deletion_protection = false
  }

  assert {
    condition = length([
      for env_var in google_cloud_run_v2_service.webapp.template[0].containers[0].env :
      env_var.name if env_var.name == "HELLO"
    ]) > 0
    error_message = "A standard environment variable with the name 'HELLO' should be created."
  }
  assert {
    condition = length([
      for env_var in google_cloud_run_v2_service.webapp.template[0].containers[0].env :
      env_var.value if env_var.value == "WORLD!"
    ]) > 0
    error_message = "A standard environment variable with the name 'WORLD!' should be created."
  }
  assert {
    condition = length([
      for env_var in google_cloud_run_v2_service.webapp.template[0].containers[0].env :
      env_var.name if env_var.name == "SECRET"
    ]) > 0
    error_message = "A secret-backed environment variable with the name 'SECRET' should be created."
  }
  assert {
    condition = [
      for env_var in google_cloud_run_v2_service.webapp.template[0].containers[0].env :
      env_var.value_source if length(env_var.value_source) > 0
    ][0][0].secret_key_ref[0].secret == run.setup.secret_ids[0]
    error_message = "A secret-backed environment variable referencing the 'run.setup.secret_ids[0]' secret should be created."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].name == "secret-volume"
    error_message = "A volume mount with the name 'secret-volume' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].volume_mounts[0].mount_path == "/secrets"
    error_message = "A volume mount with the mount path '/secrets' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].name == "secret-volume"
    error_message = "A volume with the name 'secret-volume' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].secret[0].secret == run.setup.secret_ids[1]
    error_message = "A secret-backed volume referencing the 'run.setup.secret_ids[0]' secret should be created."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].secret[0].items[0].version == "1"
    error_message = "Secret version should be '1'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].volumes[0].secret[0].items[0].path == "my-secret"
    error_message = "Secret path should be 'my-secret'."
  }
}

run "test_service_with_multiple_containers" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp1 = {
        name  = "webapp-1"
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        ports = [
          {
            container_port = 8080
          }
        ]
      }
      webapp2 = {
        name  = "webapp-2"
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = length(google_cloud_run_v2_service.webapp.template[0].containers) == 2
    error_message = "The Cloud Run service should have two containers defined."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].name == "webapp-1"
    error_message = "The first container definition should be configured with the name 'webapp-1'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[0].ports[0].container_port == 8080
    error_message = "The first container definition should be configured with a single container_port of 8080."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].containers[1].name == "webapp-2"
    error_message = "The second container definition should be configured with the name 'webapp-2'."
  }
}

run "test_traffic_distribution_across_multiple_revisions_initial_deploy" {
  variables {
    name     = run.setup.random_name
    revision = "${run.setup.random_name}-v1-0-0"
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    traffic = [
      {
        type     = "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
        revision = "${run.setup.random_name}-v1-0-0"
        percent  = 100
      }
    ]
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].revision == "${run.setup.random_name}-v1-0-0"
    error_message = "The service revision name should be '${run.setup.random_name}-v1-0-0'."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.traffic) == 1
    error_message = "There should be a single traffic block configured."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].type == "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
    error_message = "There should be a single traffic block with the type 'TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].revision == "${run.setup.random_name}-v1-0-0"
    error_message = "There should be a single traffic block with the revision '${run.setup.random_name}-v1-0-0'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].percent == 100
    error_message = "There should be a single traffic block with percent set to 100."
  }
}

run "test_traffic_distribution_across_multiple_revisions_split_50_50" {
  variables {
    name     = run.setup.random_name
    revision = "${run.setup.random_name}-v1-1-0"
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    traffic = [
      {
        type     = "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
        revision = "${run.setup.random_name}-v1-0-0"
        percent  = 50
      },
      {
        type     = "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
        revision = "${run.setup.random_name}-v1-1-0"
        percent  = 50
      }
    ]
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].revision == "${run.setup.random_name}-v1-1-0"
    error_message = "The service revision name should be '${run.setup.random_name}-v1-1-0'."
  }
  assert {
    condition     = length(google_cloud_run_v2_service.webapp.traffic) == 2
    error_message = "There should be two traffic blocks configured."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].type == "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
    error_message = "The first traffic block should be configured with the type 'TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].revision == "${run.setup.random_name}-v1-0-0"
    error_message = "The first traffic block should be configured with the revision '${run.setup.random_name}-v1-0-0'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[0].percent == 50
    error_message = "The first traffic block should be configured with percent set to 50."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[1].type == "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
    error_message = "The second traffic block should be configured with the type 'TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[1].revision == "${run.setup.random_name}-v1-1-0"
    error_message = "The second traffic block should be configured with the revision '${run.setup.random_name}-v1-1-0'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.traffic[1].percent == 50
    error_message = "The second traffic block should be configured with percent set to 50."
  }
}
