variable "project" {
  description = "The ID of a project to deploy the testing resources to."
  type        = string
}

variable "region" {
  description = "The region/location to deploy test resources to."
  type        = string
}

variable "create_test_sql_instances" {
  description = "If true, two SQL instances will be deployed for testing."
  type        = bool
  default     = false
}

variable "create_test_secrets" {
  description = "If true, two secret objects will be deployed for testing."
  type        = bool
  default     = false
}

variable "create_test_secrets_iam" {
  description = <<EOI
If true, IAM bindings will be created to allow the Cloud Run service to access the test secret objects.
EOI
  type        = bool
  default     = false
}

variable "create_vpc_connector" {
  description = "If true, create a VPC Access Connector and associated subnet for testing."
  type        = bool
  default     = false
}

variable "test_ip_cidr_range" {
  description = "CIDR range for the subnet which is created for the VPC Access Connector."
  type        = string
  default     = "10.0.0.16/28"
}
