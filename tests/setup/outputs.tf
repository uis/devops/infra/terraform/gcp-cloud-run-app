output "random_name" {
  description = "A random name to use for the test run."
  value       = random_id.name.hex
}

output "instance_connection_names" {
  description = "A list of created SQL instance connection names."
  value       = [for k, v in module.sql : v.instance_connection_name]
}

output "secret_ids" {
  description = "A list of created secret object IDs."
  value       = [for k, v in google_secret_manager_secret.test : v.id]
}

output "subnetwork_id" {
  description = "The ID of the subnetwork created to test VPC access for the Cloud Run service."
  value       = var.create_vpc_connector ? google_compute_subnetwork.test[0].id : null
}

output "vpc_connector_id" {
  description = "The ID of the VPC Access Connector object created to test VPC access for the Cloud Run service."
  value       = var.create_vpc_connector ? google_vpc_access_connector.test[0].id : null
}
