# These are test resources which are destroyed after each test run. Therefore, we are disabling kics-scan on the whole
# file.
# kics-scan ignore

resource "random_id" "name" {
  byte_length = 2
  # "rapp" represents Cloud Run App and is required to ensure any resources created by this repo's tests are easily
  # identifiable by the cleanup.sh script. We only have 4 characters to play with given some of Google's naming
  # restrictions.
  prefix = "test-rapp"
}

module "sql" {
  count = var.create_test_sql_instances ? 2 : 0

  source                      = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version                     = "~> 23.0"
  database_version            = "POSTGRES_15"
  name                        = "${random_id.name.hex}-${count.index}"
  project_id                  = var.project
  tier                        = "db-f1-micro"
  availability_type           = "ZONAL"
  region                      = var.region
  zone                        = "${var.region}-a"
  deletion_protection         = false
  deletion_protection_enabled = false
}

resource "google_secret_manager_secret" "test" {
  count = var.create_test_secrets ? 2 : 0

  secret_id = "${random_id.name.hex}-secret-version-${count.index}"
  project   = var.project

  replication {
    auto {}
  }
}

resource "google_secret_manager_secret_version" "test" {
  count = var.create_test_secrets ? 2 : 0

  secret      = google_secret_manager_secret.test[count.index].id
  secret_data = "secret-data"
}

resource "google_secret_manager_secret_iam_member" "test" {
  count = var.create_test_secrets_iam ? 2 : 0

  project   = var.project
  secret_id = google_secret_manager_secret.test[count.index].id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${random_id.name.hex}-run@${var.project}.iam.gserviceaccount.com"
}

resource "google_compute_subnetwork" "test" {
  count = var.create_vpc_connector ? 1 : 0

  name          = "${random_id.name.hex}-setup"
  project       = var.project
  ip_cidr_range = var.test_ip_cidr_range
  network       = "default"
  region        = var.region
}

resource "google_vpc_access_connector" "test" {
  count = var.create_vpc_connector ? 1 : 0

  min_throughput = 200
  max_throughput = 300

  name    = "${random_id.name.hex}-setup"
  project = var.project
  region  = var.region

  subnet {
    name = google_compute_subnetwork.test[0].name
  }
}
