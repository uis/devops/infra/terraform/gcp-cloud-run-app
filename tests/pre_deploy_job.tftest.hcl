run "setup" {
  variables {
    create_test_secrets = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_pre_deploy_job" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_pre_deploy_job = true
    pre_deploy_job_container = {
      image   = "golang:latest"
      command = ["go"]
      args    = ["version"]
    }
    deletion_protection = false
  }
}

run "setup_secret_iam" {
  variables {
    create_test_secrets     = true
    create_test_secrets_iam = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_pre_deploy_job_env_vars_and_secrets" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_pre_deploy_job = true
    pre_deploy_job_container = {
      image   = "golang:latest"
      command = ["go"]
      args    = ["version"]
      env = [
        {
          name  = "HELLO",
          value = "WORLD!"
        },
        {
          name = "SECRET",
          value_source = {
            secret_key_ref = {
              secret = run.setup.secret_ids[0]
            }
          }
        }
      ]
      volume_mounts = [
        {
          name       = "secret-volume",
          mount_path = "/secrets"
        }
      ]
    }
    pre_deploy_job_volumes = [
      {
        name = "secret-volume",
        secret = {
          secret = run.setup.secret_ids[1]
          items = [
            {
              version = "1",
              path    = "my-secret"
            }
          ]
        }
      }
    ]
    deletion_protection = false
  }

  assert {
    condition = length([
      for env_var in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].env :
      env_var.name if env_var.name == "HELLO"
    ]) > 0
    error_message = "A standard environment variable with the name 'HELLO' should be created."
  }
  assert {
    condition = length([
      for env_var in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].env :
      env_var.value if env_var.value == "WORLD!"
    ]) > 0
    error_message = "A standard environment variable with the name 'WORLD!' should be created."
  }
  assert {
    condition = length([
      for env_var in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].env :
      env_var.name if env_var.name == "SECRET"
    ]) > 0
    error_message = "A secret-backed environment variable with the name 'SECRET' should be created."
  }
  assert {
    condition = [
      for env_var in google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].env :
      env_var.value_source if length(env_var.value_source) > 0
    ][0][0].secret_key_ref[0].secret == run.setup.secret_ids[0]
    error_message = "A secret-backed environment variable referencing the 'run.setup.secret_ids[0]' secret should be created."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].name == "secret-volume"
    error_message = "A volume mount with the name 'secret-volume' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].containers[0].volume_mounts[0].mount_path == "/secrets"
    error_message = "A volume mount with the mount path '/secrets' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].name == "secret-volume"
    error_message = "A volume with the name 'secret-volume' should be created."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].secret[0].secret == run.setup.secret_ids[1]
    error_message = "A secret-backed volume referencing the 'run.setup.secret_ids[0]' secret should be created."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].secret[0].items[0].version == "1"
    error_message = "Secret version should be '1'."
  }
  assert {
    condition     = google_cloud_run_v2_job.pre_deploy[0].template[0].template[0].volumes[0].secret[0].items[0].path == "my-secret"
    error_message = "Secret path should be 'my-secret'."
  }
}
