run "setup" {
  module {
    source = "./tests/setup"
  }
}

run "test_monitoring_with_alert_policies_created_in_default_project" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_monitoring   = true
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.ingress == "INGRESS_TRAFFIC_ALL"
    error_message = "Ingress should be 'INGRESS_TRAFFIC_ALL'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].ssl_cert_expiry_alert_policy.enabled == true
    error_message = "SSL cert expiry alert policy should be enabled."
  }
  assert {
    condition     = module.uptime_monitoring["default"].ssl_cert_expiry_alert_policy.project == var.project
    error_message = "SSL cert expiry alert policy should be created in the project specified in var.project."
  }
  assert {
    condition     = module.uptime_monitoring["default"].uptime_check_alert_policy.enabled == true
    error_message = "Uptime alert policy should be enabled."
  }
  assert {
    condition     = module.uptime_monitoring["default"].uptime_check_alert_policy.project == var.project
    error_message = "Uptime alert policy should be created in the project specified in var.project."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.period == "300s"
    error_message = "Uptime check should be configured with period == '300s'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.timeout == "30s"
    error_message = "Uptime check should be configured with timeout == '30s'."
  }
  assert {
    condition     = length(module.uptime_monitoring["default"].https_uptime_check_config.http_check) == 1
    error_message = "A single HTTP check should be configured."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].path == "/"
    error_message = "The HTTP check path should be '/'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].port == 443
    error_message = "The HTTP check port should be 443."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].request_method == "GET"
    error_message = "The HTTP check request method should be 'GET'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].use_ssl == true
    error_message = "The HTTP check should be configured to use SSL."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].validate_ssl == true
    error_message = "The HTTP check should be configured to validate SSL."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.monitored_resource[0].type == "uptime_url"
    )
    error_message = "The uptime check should be configured to monitor a URL resource."
  }
}

run "test_monitoring_with_alert_policies_created_in_scoping_project" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_monitoring          = true
    monitoring_scoping_project = "infra-testing-meta-21f09a44"
    deletion_protection        = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.ingress == "INGRESS_TRAFFIC_ALL"
    error_message = "Ingress should be 'INGRESS_TRAFFIC_ALL'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].ssl_cert_expiry_alert_policy.enabled == true
    error_message = "SSL cert expiry alert policy should be enabled."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].ssl_cert_expiry_alert_policy.project == var.monitoring_scoping_project
    )
    error_message = (
      "SSL cert expiry alert policy should be created in the project specified in var.monitoring_scoping_project."
    )
  }
  assert {
    condition     = module.uptime_monitoring["default"].uptime_check_alert_policy.enabled == true
    error_message = "Uptime alert policy should be enabled."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].uptime_check_alert_policy.project == var.monitoring_scoping_project
    )
    error_message = "Uptime alert policy should be created in the project specified in var.monitoring_scoping_project."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.period == "300s"
    error_message = "Uptime check should be configured with period == '300s'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.timeout == "30s"
    error_message = "Uptime check should be configured with timeout == '30s'."
  }
  assert {
    condition     = length(module.uptime_monitoring["default"].https_uptime_check_config.http_check) == 1
    error_message = "A single HTTP check should be configured."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].path == "/"
    error_message = "The HTTP check path should be '/'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].port == 443
    error_message = "The HTTP check port should be 443."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].request_method == "GET"
    error_message = "The HTTP check request method should be 'GET'."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].use_ssl == true
    error_message = "The HTTP check should be configured to use SSL."
  }
  assert {
    condition     = module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].validate_ssl == true
    error_message = "The HTTP check should be configured to validate SSL."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.monitored_resource[0].type == "uptime_url"
    )
    error_message = "The uptime check should be configured to monitor a URL resource."
  }
}

run "test_monitoring_with_auth_proxy" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_monitoring                 = true
    allow_unauthenticated_invocations = false
    deletion_protection               = false
  }

  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.monitored_resource[0].labels.host
      == module.uptime_monitoring["default"].auth_proxy_host
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function host."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].path
      == module.uptime_monitoring["default"].auth_proxy_path
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function path."
  }
  assert {
    condition     = module.uptime_monitoring["default"].auth_proxy_function.ingress_settings == "ALLOW_ALL"
    error_message = "The authentication proxy function should be configured to allow all ingress."
  }
}

run "test_monitoring_with_auth_proxy_and_vpc_access_connector" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_monitoring                 = true
    allow_unauthenticated_invocations = false
    ingress                           = "INGRESS_TRAFFIC_INTERNAL_ONLY"
    deletion_protection               = false
  }

  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.monitored_resource[0].labels.host
      == module.uptime_monitoring["default"].auth_proxy_host
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function host."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].path
      == module.uptime_monitoring["default"].auth_proxy_path
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function path."
  }
  assert {
    condition = (
      google_compute_subnetwork.vpc_connector[0].network
      == "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/default"
    )
    error_message = "A subnet for the VPC connector should be created in the 'default' network."
  }
  assert {
    condition     = google_vpc_access_connector.main[0].project == var.project
    error_message = "A VPC connector should be created in the specified project."
  }
  assert {
    condition     = module.uptime_monitoring["default"].auth_proxy_function.ingress_settings == "ALLOW_ALL"
    error_message = "The authentication proxy function should be configured to allow all ingress."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].auth_proxy_function.vpc_connector
      == "projects/${var.project}/locations/${var.region}/connectors/${var.name}-conn"
    )
    error_message = "The authentication proxy function should be configured to use the created VPC connector."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].auth_proxy_function.vpc_connector_egress_settings == "ALL_TRAFFIC"
    )
    error_message = (
      "The authentication proxy function should be configured to route all egress traffic via the VPC connector."
    )
  }
}

run "setup_vpc_connector" {
  variables {
    create_vpc_connector = true
  }

  module {
    source = "./tests/setup"
  }
}

run "test_monitoring_with_auth_proxy_and_an_existing_vpc_access_connector" {
  variables {
    name = run.setup.random_name
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    enable_monitoring                 = true
    allow_unauthenticated_invocations = false
    ingress                           = "INGRESS_TRAFFIC_INTERNAL_ONLY"
    vpc_access = {
      connector = run.setup_vpc_connector.vpc_connector_id
      egress    = "ALL_TRAFFIC"
    }
    deletion_protection = false
  }

  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.monitored_resource[0].labels.host
      == module.uptime_monitoring["default"].auth_proxy_host
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function host."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].https_uptime_check_config.http_check[0].path
      == module.uptime_monitoring["default"].auth_proxy_path
    )
    error_message = "HTTP check path should be configured to use the authentication proxy function path."
  }
  assert {
    condition     = try(google_compute_subnetwork.vpc_connector[0], null) == null
    error_message = "A subnet should not be created."
  }
  assert {
    condition     = try(google_vpc_access_connector.main[0], null) == null
    error_message = "A VPC connector should not be created."
  }
  assert {
    condition     = module.uptime_monitoring["default"].auth_proxy_function.ingress_settings == "ALLOW_ALL"
    error_message = "The authentication proxy function should be configured to allow all ingress."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].auth_proxy_function.vpc_connector
      == "projects/${var.project}/locations/${var.region}/connectors/${var.name}-setup"
    )
    error_message = "The authentication proxy function should be configured to use the provided VPC connector."
  }
  assert {
    condition = (
      module.uptime_monitoring["default"].auth_proxy_function.vpc_connector_egress_settings == "ALL_TRAFFIC"
    )
    error_message = (
      "The authentication proxy function should be configured to route all egress traffic via the VPC connector."
    )
  }
}
