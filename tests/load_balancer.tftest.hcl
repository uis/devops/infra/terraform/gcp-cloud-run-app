run "setup" {
  module {
    source = "./tests/setup"
  }
}

run "test_service_with_default_variable_values_and_load_balancer_enabled" {
  variables {
    name                 = run.setup.random_name
    enable_load_balancer = true
    dns_names = {
      webapp = "${run.setup.random_name}.test.example.gcp.uis.cam.ac.uk"
    }
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.ingress == "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER"
    error_message = "Ingress should be 'INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.launch_stage == "GA"
    error_message = "Launch stage should be 'GA'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.location == "europe-west2"
    error_message = "Location should be 'europe-west2'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].execution_environment == "EXECUTION_ENVIRONMENT_GEN1"
    error_message = "Execution environment should be 'EXECUTION_ENVIRONMENT_GEN1'."
  }
  assert {
    condition     = google_cloud_run_v2_service.webapp.template[0].timeout == "300s"
    error_message = "Timeout should be '300s'."
  }
  assert {
    condition     = google_cloud_run_v2_service_iam_member.webapp_all_users_invoker[0].role == "roles/run.invoker"
    error_message = "google_cloud_run_v2_service_iam_member.webapp_all_users_invoker.role should be 'roles/run.invoker'."
  }
  assert {
    condition     = google_cloud_run_v2_service_iam_member.webapp_all_users_invoker[0].member == "allUsers"
    error_message = "google_cloud_run_v2_service_iam_member.webapp_all_users_invoker.member should be 'allUsers'."
  }
  assert {
    condition     = google_service_account.webapp.name != null
    error_message = "A dedicated service account should be created for the Cloud Run service."
  }
  assert {
    condition     = google_compute_region_network_endpoint_group.webapp[0].network_endpoint_type == "SERVERLESS"
    error_message = "Network endpoint group type should be 'SERVERLESS'."
  }
  assert {
    condition     = google_compute_region_network_endpoint_group.webapp[0].cloud_run[0].service == google_cloud_run_v2_service.webapp.name
    error_message = "Network endpoint group must contain the created Cloud Run service in its definition."
  }
  assert {
    condition     = google_compute_ssl_policy.default[0].min_tls_version == "TLS_1_2"
    error_message = "Minimum TLS version should be 'TLS_1_2'."
  }
  assert {
    condition     = google_compute_ssl_policy.default[0].profile == "MODERN"
    error_message = "SSL profile should be 'MODERN'."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].protocol == "HTTP"
    error_message = "The default backend service protocol should be 'HTTP'."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].enable_cdn == false
    error_message = "The default backend service should not enable CDN."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].log_config[0].enable == true
    error_message = "The default backend service log config should be enabled."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].log_config[0].sample_rate == 1.0
    error_message = "The default backend service log sample rate should be '1.0'."
  }
  assert {
    condition     = length(module.webapp_http_load_balancer[0].backend_services["default"].backend) == 1
    error_message = "The default backend service should be configured with a single backend block."
  }
  assert {
    condition = contains([
      for backend in module.webapp_http_load_balancer[0].backend_services["default"].backend : backend.group
    ], google_compute_region_network_endpoint_group.webapp[0].self_link)
    error_message = "The default backend service group should be configured to use the created network_endpoint_group."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].http_proxy != null
    error_message = "A http proxy resource should be created by the load balancer module."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].https_proxy != null
    error_message = "A https proxy resource should be created by the load balancer module."
  }
}

run "test_service_with_load_balancer_enabled_and_ingress_set_to_allow_all" {
  variables {
    name                 = run.setup.random_name
    enable_load_balancer = true
    ingress              = "INGRESS_TRAFFIC_ALL"
    dns_names = {
      webapp = "${run.setup.random_name}.test.example.gcp.uis.cam.ac.uk"
    }
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = google_cloud_run_v2_service.webapp.ingress == "INGRESS_TRAFFIC_ALL"
    error_message = "Ingress should be 'INGRESS_TRAFFIC_ALL'."
  }
}

run "test_service_with_load_balancer_enabled_and_load_balancer_backend_overrides" {
  variables {
    name                 = run.setup.random_name
    enable_load_balancer = true
    dns_names = {
      webapp = "${run.setup.random_name}.test.example.gcp.uis.cam.ac.uk"
    }
    load_balancer_backend = {
      description = "Lift, Load, Balance!"
      log_config = {
        enable      = true
        sample_rate = 0.5
      }
    }
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
  }

  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].log_config[0].sample_rate == 0.5
    error_message = "The default backend service log sample rate should be '0.5'."
  }
  assert {
    condition     = module.webapp_http_load_balancer[0].backend_services["default"].description == "Lift, Load, Balance!"
    error_message = "The default backend service description should be 'Lift, Load, Balance!'."
  }
}

run "test_service_with_load_balancer_enabled_and_load_balancer_alerting_defaults" {
  variables {
    name                 = run.setup.random_name
    enable_load_balancer = true
    dns_names = {
      webapp = "${run.setup.random_name}.test.example.gcp.uis.cam.ac.uk"
    }
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
    alerting_load_balancer_server_errors = {
      enabled = true
    }
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0] != null
    error_message = "An alert policy resource should be created."
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0].enabled == true
    error_message = "Load balancer alert policy should be enabled."
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0].conditions[0].condition_threshold[0].threshold_value == 10
    error_message = "Load balancer alert condition triggers after 10 5XX responses."
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0].conditions[0].condition_threshold[0].aggregations[0].alignment_period == "60s"
    error_message = "Load balancer alert condition aggregates over 60s."
  }
}

run "test_service_with_load_balancer_enabled_and_load_balancer_alerting" {
  variables {
    name                 = run.setup.random_name
    enable_load_balancer = true
    dns_names = {
      webapp = "${run.setup.random_name}.test.example.gcp.uis.cam.ac.uk"
    }
    containers = {
      webapp = {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
    deletion_protection = false
    alerting_load_balancer_server_errors = {
      enabled = true
      alignment_period = "120s"
      threshold = 100
    }
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0] != null
    error_message = "An alert policy resource should be created."
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0].conditions[0].condition_threshold[0].threshold_value == 100
    error_message = "Load balancer alert condition triggers after 100 5XX responses."
  }

  assert {
    condition = google_monitoring_alert_policy.load_balancer_server_error_alerts[0].conditions[0].condition_threshold[0].aggregations[0].alignment_period == "120s"
    error_message = "Load balancer alert condition aggregates over 120s."
  }
}
