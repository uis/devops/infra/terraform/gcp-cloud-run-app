# Changelog

## [10.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/10.0.1...10.1.0) (2025-03-03)

### Features

* add optional load balancer alerting ([0d4343e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/0d4343ecbd55c378ece381fb8a598b04e283f8ef))
* replace versions.tf.json with versions.tf ([28a0d04](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/28a0d046cccf188569aaaa3c7844b7f63fe51429))

## [10.0.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/10.0.0...10.0.1) (2025-01-10)

### Bug Fixes

* local_files_dir variable workaround ([1de2488](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/1de2488c5175fd0b68db80b5c38fe82ef3ebe0bf))
* set delault value of deletion_protection variable to 'true' ([03e5a52](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/03e5a52ba3461b98a6ac186797b30339746816cf))

## [10.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.2.0...10.0.0) (2024-11-18)

### ⚠ BREAKING CHANGES

* **deps:** Remove the `use_ssl_certificates` variable as the load
balancer module no longer accepts it.

Update tests and examples to use a more recent version (v23) of the SQL
module to resolve provider version conflicts.

Modify tests to only verify Google provider versions >= 6 as this is the
minimum version supported by Google's load balancer module.

### Bug Fixes

* tests after provider version upgrade ([872333d](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/872333dcf538b3de01ad812e5e7f9b190597829a))

### Miscellaneous Chores

* **deps:** update terraform googlecloudplatform/lb-http/google to v12 ([f315654](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/f315654173129805698b40af0191b01c0665fbe7))

## [9.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.1.2...9.2.0) (2024-10-01)


### Features

* switch to v5.0.x of gcp-site-monitoring to ensure we're using supported version of Python ([4d5ee11](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/4d5ee11ae6ff003b9dc825446e36e93b16c3cde9))


### Bug Fixes

* the way run_tests.sh works with arguments ([f94ec5e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/f94ec5e2fe670659520157b86ad75ee343d1a3dc))

## [9.1.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.1.1...9.1.2) (2024-08-15)

## [9.1.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.1.0...9.1.1) (2024-07-24)

## [9.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.0.1...9.1.0) (2024-07-15)


### Features

* added load_balancer_backend variable to provide any custom options ([80605d8](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/80605d893a34c4d7b27e23fc4b1e42487fe709bf))

## [9.0.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/9.0.0...9.0.1) (2024-06-13)


### Bug Fixes

* allow ingress override when using load balancer ([b2c6777](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/b2c67775675eaea85f680424edb8ea71e99d337d))

## [9.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/compare/8.4.0...9.0.0) (2024-02-28)


### ⚠ BREAKING CHANGES

* Migrate to Cloud Run v2 resource. This includes changes to many of the input variables. For
  guidance see our [migration guide](https://guidebook.devops.uis.cam.ac.uk/howtos/migrate-a-gcp-cloud-run-app-terraform-deployment-to-v9/#update-the-module-version).
* Disabled monitoring by default.
* Remove Cloud Run Domain Mapping.

### Features

* migrate to use cloud run v2 resources ([041dc74](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/041dc7404f1acce64fd411cdbb7f8901d073e9b2))
* refactor uptime_monitoring module ([180e3e9](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/180e3e9c41ff8951718217f448546d01041616b7))
* remove cloud run domain mapping ([02ed8b0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/02ed8b0795f2b11aed6f258ba888cd0008c77cce))
* remove legacy dashboard resources ([12ea106](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/commit/12ea106d6bc9cf921da873166c31f6cb5a5e3908))

## [8.4.0] - 2023-09-11

### Added

- new variable `vpc_access_connector_max_throughput` to allow max_throughput to be set on the vpc_access_connector resource.

## [8.3.0] - 2023-08-18

### Added

- Include terraform-module.yml pipeline to publish new versions of this module to the internal Terraform Registry.

## [8.2.0] - 2023-07-04

### Added

- Added new variable `pre_deploy_job_environment_variables` to optionally override `environment_variables` for pre-deploy jobs.

## [8.1.0] - 2023-06-20

### Added

- Added new `pre-deploy` job to run tasks such as database migrations before deploying a new version of the webapp.

## [8.0.0] - 2023-06-16

### Added

- BREAKING CHANGE: Add a default SSL policy restricting TLS to >= 1.2 and setting the SSL profile to MODERN.
- BREAKING CHANGE: Upgrade to v3 of the gcp-site-monitoring module. This could cause previously deployed uptime check functions to be destroyed/re-created during the next Terraform apply.

### Fixed

- Add new annotations and labels to the `ignore_changes` map in the `lifecycle` block of the `google_cloud_run_service.webapp` resource.

## [7.1.0] - 2023-05-15

### Changed
- Added the load balancer(s) as an output from the module.
- Added new variable to pass in an `ssl_policy` self-link that is used in the load balancer
  (if present).

## [7.0.0] - 2022-12-19

### Fixed

- Issue #39 - Broken `for_each` on initial deployment.
  - This is a **BREAKING CHANGE** and will force previously deployed load balancers to be redeployed.
  You can work around this by using a [moved](https://developer.hashicorp.com/terraform/language/modules/develop/refactoring#moved-block-syntax)
  block in the calling terraform configuration when moving to v7.x of this module.

## [6.0.0] - 2022-11-09
### Changed
- Migrate `google.stackdriver` provider alias to use the latest `configuration_aliases` syntax.
- Update `dashboard.tf` to use `templatefile` built-in func.
- Use boolean variable to grant sql service account permissions.
  - This is a **BREAKING CHANGE**. Users will need to add `grant_sql_client_role_to_webapp_sa = true`
  to module calls that require the `roles/cloudsql.client` role to be granted to the webapp service account.
  This was previously determined by the `sql_instance_connection_name` variable, however, this was problematic when the value
  was unknown until after a terraform apply.

### Fixed
- Fix local.monitor_hosts issue caused by map key values being unknown.

## [5.0.0] - 2022-05-23 ??
### Changed
 - Made permitted versions of google providers and terraform itself more broad.

## [4.1.5] - 2022-03-30
### Added
 - Ensure default -> null vpc_connector network changes are not applied on every deploy

## [4.1.4] - 2022-03-21
### Added
 - Remove Beta launch stage flag that was required for referencing Secret Manager
   secrets. See https://cloud.google.com/run/docs/release-notes#November_09_2021

## [4.1.3] - 2022-01-24
### Added
 - Add a monitoring dashboard, defined in ./dashboard.json, for the Cloud Run service.
   The dashboard contains charts for Request count and latency, CPU and memory utilisation,
   and container instance count and billable time.

## [4.1.2] - 2022-01-10
### Fixed
 - Correct logic used to decide if custom domains can be monitored.

## [4.1.1] - 2022-01-06
### Fixed
 - Remove use of hard-coded resource name in data resource.

## [4.1.0] - 2021-07-28
### Added
 - Support load balancer ingress style alongside Cloud Run domain mapping.

## [4.0.1] - 2021-07-15
### Changed
 - Surface Cloud NAT variable for minimum number of SNAT tuples, supporting a larger
   number of outbound connections if required.

## [4.0.0] - 2021-06-29
### Changed
 - Add the requirement for an explicit image_name to deploy, which breaks previous
   versions that ignored image updates.

## [3.1.3] - 2021-07-16
### Changed
 - Added interface for authentication proxy Cloud Function egress settings. Required
   for uptime check configuration of internal services.

## [3.1.2] - 2021-07-15
### Changed
 - Surface Cloud NAT variable for minimum number of SNAT tuples, supporting a larger
   number of outbound connections id required.

## [3.1.1] - 2021-06-16
### Changed
 - Surface the alerting success threshold variable from the monitoring module.

## [3.1.0] - 2021-06-16
### Changed
 - Cloud Run service account is given permissions to access all Secret Manager secrets
   passed via secrets_envars and secrets_volumes variables.

## [3.0.0] - 2021-06-08
### Added
 - Raised the provider to 3.70, adding the BETA requirement and the capability to
   directly expose secrets manager secrets as files and env vars.

## [2.0.5] - 2021-05-28
### Changed
 - Remove the requirement of BETA staging when the minimum instance number is above 0
   or when using internal or load balancer ingresses.

## [2.0.4] - 2021-04-19
### Changed
 - Replace 'placeholder' with 'gvisor' for 'run.googleapis.com/sandbox' annotation

## [2.0.3] - 2021-04-15
### Changed
 - Ensure that static egress ips cannot be accidentally destroyed.

## [2.0.2] - 2021-04-07
### Changed
 - Allow cloud run instances which required authentication to be monitored.

## [2.0.1] - 2021-03-09
### Changed
 - Add some more attributes to those ignored to reduce terraform noise in plans.

## [2.0.0] - 2021-01-27
### Added
 - Initial version
