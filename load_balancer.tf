# load_balancer.tf configures Cloud Load Balancer resources for the Cloud Run service if
# var.enable_load_balancer == true.

# A network endpoint group for the "webapp" application.
resource "google_compute_region_network_endpoint_group" "webapp" {
  count = var.enable_load_balancer ? 1 : 0

  name                  = var.name
  project               = var.project
  network_endpoint_type = "SERVERLESS"
  region                = var.region
  cloud_run {
    service = google_cloud_run_v2_service.webapp.name
  }
}

resource "google_compute_ssl_policy" "default" {
  count = var.enable_load_balancer && var.ssl_policy == null ? 1 : 0

  name            = "${var.name}-modern"
  project         = var.project
  profile         = "MODERN"
  min_tls_version = "TLS_1_2"
}

# A load balancer for the "webapp" application. This is just a set of sane
# defaults. See the full documentation at [1] for customisation.
#
# [1] https://registry.terraform.io/modules/GoogleCloudPlatform/lb-http/google/latest/submodules/serverless_negs
module "webapp_http_load_balancer" {
  count = var.enable_load_balancer ? 1 : 0

  # The double slash is important(!)
  source  = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version = "~> 12.0"

  project = var.project
  name    = var.name

  ssl                             = true
  managed_ssl_certificate_domains = [for k, v in var.dns_names : v]
  ssl_policy                      = var.ssl_policy == null ? google_compute_ssl_policy.default[0].id : var.ssl_policy
  https_redirect                  = true

  # Specify custom TLS certs, otherwise the Google-managed certs will be used.
  ssl_certificates = var.ssl_certificates

  # Whether to create an IPv6 address to the load balancer.
  enable_ipv6         = var.enable_ipv6
  create_ipv6_address = var.create_ipv6_address

  backends = {
    default = {
      protocol = "HTTP"

      groups = [
        {
          group = google_compute_region_network_endpoint_group.webapp[0].id
        }
      ]

      # Currently Cloud IAP is not supported for Cloud Run endpoints. We still
      # need to specify that we don't want to use it though :).
      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }

      #
      # Optional settings for the backend service.
      #
      port_name               = var.load_balancer_backend.port_name
      description             = var.load_balancer_backend.description
      enable_cdn              = var.load_balancer_backend.enable_cdn
      compression_mode        = var.load_balancer_backend.compression_mode
      security_policy         = var.load_balancer_backend.security_policy
      edge_security_policy    = var.load_balancer_backend.edge_security_policy
      custom_request_headers  = var.load_balancer_backend.custom_request_headers
      custom_response_headers = var.load_balancer_backend.custom_response_headers

      connection_draining_timeout_sec = var.load_balancer_backend.connection_draining_timeout_sec
      session_affinity                = var.load_balancer_backend.session_affinity
      affinity_cookie_ttl_sec         = var.load_balancer_backend.affinity_cookie_ttl_sec
      locality_lb_policy              = var.load_balancer_backend.locality_lb_policy

      log_config = coalesce(var.load_balancer_backend.log_config,
        {
          enable      = true
          sample_rate = 1.0
        }
      )

      cdn_policy        = var.load_balancer_backend.cdn_policy
      outlier_detection = var.load_balancer_backend.outlier_detection
    }
  }
}

locals {
  default_server_error_alert_policy_name = "${var.name} Load Balancer: 5xx responses"

  server_error_alert_policy_name = (
    var.alerting_load_balancer_server_errors.policy_name != null
    ? var.alerting_load_balancer_server_errors.policy_name
    : local.default_server_error_alert_policy_name
  )

  final_server_error_alert_policy_name = (
    var.alerting_load_balancer_server_errors.policy_name_prefix != null
    ? join(" ", [
      var.alerting_load_balancer_server_errors.policy_name_prefix,
      local.server_error_alert_policy_name
    ])
    : local.server_error_alert_policy_name
  )

  server_error_alert_condition_name = (
    var.alerting_load_balancer_server_errors.condition_name != null
    ? var.alerting_load_balancer_server_errors.condition_name
    : "5xx responses > ${var.alerting_load_balancer_server_errors.threshold} in ${var.alerting_load_balancer_server_errors.alignment_period}"
  )
}

resource "google_monitoring_alert_policy" "load_balancer_server_error_alerts" {
  count = var.enable_load_balancer && var.alerting_load_balancer_server_errors.enabled ? 1 : 0

  project      = coalesce(var.monitoring_scoping_project, var.project)
  display_name = local.final_server_error_alert_policy_name
  combiner     = "OR"

  enabled = true

  notification_channels = var.alerting_notification_channels

  conditions {
    display_name = local.server_error_alert_condition_name

    condition_threshold {
      filter = <<-EOI
        metric.type="loadbalancing.googleapis.com/https/request_count"
        AND resource.type="https_lb_rule"
        AND resource.label.project_id="${var.project}"
        AND resource.label.backend_name="${var.name}"
        AND metric.labels.response_code_class="500"
      EOI
      # We check the threshold every minute
      duration        = "60s"
      comparison      = "COMPARISON_GT"
      threshold_value = var.alerting_load_balancer_server_errors.threshold
      aggregations {
        alignment_period     = var.alerting_load_balancer_server_errors.alignment_period
        per_series_aligner   = "ALIGN_SUM"
        cross_series_reducer = "REDUCE_SUM"
      }
      evaluation_missing_data = "EVALUATION_MISSING_DATA_INACTIVE"
    }
  }

  documentation {
    subject   = "Load balancer for cloud run ${var.name} in ${var.project} returning multiple 5XX responses"
    mime_type = "text/markdown"
    content   = <<-EOI
      The load balancer configured for traffic to the cloud run service ${var.name} has reported
      more than ${var.alerting_load_balancer_server_errors.threshold} in
      ${var.alerting_load_balancer_server_errors.alignment_period}. This could be the result of
      a fault in the deployed cloud run service.

      These alerts are created through the [DevOps GCP Cloud Run Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app).
      They can be configured or disabled through the variable arguments to this module.
    EOI
  }
}
