# main.tf configures top-level resources

# This data source pulls the configuration for the default google provider.
data "google_client_config" "current" {}

# A service account which the webapp runs in the context of.
resource "google_service_account" "webapp" {
  project      = var.project
  account_id   = coalesce(var.service_account_id, "${var.name}-run")
  display_name = coalesce(var.service_account_display_name, "Web application Cloud Run service account")
}

# Grant the webapp service account the ability to connect to the SQL instance
# via the grant_sql_client_role_to_webapp_sa boolean variable.
resource "google_project_iam_member" "webapp_sql_client" {
  count = var.grant_sql_client_role_to_webapp_sa ? 1 : 0

  project = local.sql_instance_project
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.webapp.email}"
}

resource "google_cloud_run_v2_service" "webapp" {
  name                = var.name
  location            = var.region
  description         = var.description
  project             = var.project
  labels              = var.service_labels
  client              = "terraform"
  ingress             = local.ingress
  launch_stage        = var.launch_stage
  annotations         = var.service_annotations
  deletion_protection = var.deletion_protection

  template {
    revision                         = var.revision
    labels                           = var.template_labels
    annotations                      = var.template_annotations
    timeout                          = var.timeout_seconds
    service_account                  = google_service_account.webapp.email
    execution_environment            = var.execution_environment
    encryption_key                   = var.encryption_key
    max_instance_request_concurrency = var.max_instance_request_concurrency
    session_affinity                 = var.session_affinity

    dynamic "scaling" {
      for_each = var.scaling != null ? [var.scaling] : []
      content {
        min_instance_count = scaling.value["min_instance_count"]
        max_instance_count = scaling.value["max_instance_count"]
      }
    }
    dynamic "vpc_access" {
      for_each = local.vpc_access != null ? [local.vpc_access] : []
      content {
        connector = vpc_access.value["connector"]
        egress    = vpc_access.value["egress"]
        dynamic "network_interfaces" {
          for_each = vpc_access.value["network_interfaces"] != null ? [vpc_access.value["network_interfaces"]] : []
          iterator = network_interface
          content {
            network    = network_interface.value["network"]
            subnetwork = network_interface.value["subnetwork"]
          }
        }
      }
    }
    dynamic "containers" {
      for_each = var.containers
      iterator = container
      content {
        name        = container.value["name"]
        image       = container.value["image"]
        command     = container.value["command"]
        args        = container.value["args"]
        working_dir = container.value["working_dir"]
        dynamic "env" {
          for_each = container.value["env"]
          content {
            name  = env.value["name"]
            value = env.value["value"]
            dynamic "value_source" {
              for_each = env.value["value_source"] != null ? [env.value["value_source"]] : []
              content {
                dynamic "secret_key_ref" {
                  for_each = value_source.value["secret_key_ref"] != null ? [value_source.value["secret_key_ref"]] : []
                  content {
                    secret  = secret_key_ref.value["secret"]
                    version = secret_key_ref.value["version"]
                  }
                }
              }
            }
          }
        }
        dynamic "resources" {
          for_each = container.value["resources"] != null ? [container.value["resources"]] : []
          iterator = resource
          content {
            limits            = resource.value["limits"]
            cpu_idle          = resource.value["cpu_idle"]
            startup_cpu_boost = resource.value["startup_cpu_boost"]
          }
        }
        dynamic "ports" {
          for_each = container.value["ports"]
          iterator = port
          content {
            name           = port.value["name"]
            container_port = port.value["container_port"]
          }
        }
        dynamic "volume_mounts" {
          for_each = container.value["volume_mounts"]
          iterator = volume_mount
          content {
            name       = volume_mount.value["name"]
            mount_path = volume_mount.value["mount_path"]
          }
        }
        dynamic "volume_mounts" {
          for_each = var.mount_cloudsql_instance != null ? [1] : []
          iterator = instance
          content {
            name       = "cloudsql"
            mount_path = "/cloudsql"
          }
        }
        dynamic "liveness_probe" {
          for_each = container.value["liveness_probe"] != null ? [container.value["liveness_probe"]] : []
          content {
            initial_delay_seconds = liveness_probe.value["initial_delay_seconds"]
            timeout_seconds       = liveness_probe.value["timeout_seconds"]
            period_seconds        = liveness_probe.value["period_seconds"]
            failure_threshold     = liveness_probe.value["failure_threshold"]
            dynamic "http_get" {
              for_each = liveness_probe.value["http_get"] != null ? [liveness_probe.value["http_get"]] : []
              content {
                path = http_get.value["path"]
                port = http_get.value["port"]
                dynamic "http_headers" {
                  for_each = http_get.value["http_headers"]
                  iterator = http_header
                  content {
                    name  = http_header.value["name"]
                    value = http_header.value["value"]
                  }
                }
              }
            }
            dynamic "grpc" {
              for_each = liveness_probe.value["grpc"] != null ? [liveness_probe.value["grpc"]] : []
              content {
                port    = grpc.value["port"]
                service = grpc.value["service"]
              }
            }
          }
        }
        dynamic "startup_probe" {
          for_each = container.value["startup_probe"] != null ? [container.value["startup_probe"]] : []
          content {
            initial_delay_seconds = startup_probe.value["initial_delay_seconds"]
            timeout_seconds       = startup_probe.value["timeout_seconds"]
            period_seconds        = startup_probe.value["period_seconds"]
            failure_threshold     = startup_probe.value["failure_threshold"]
            dynamic "http_get" {
              for_each = startup_probe.value["http_get"] != null ? [startup_probe.value["http_get"]] : []
              content {
                path = http_get.value["path"]
                port = http_get.value["port"]
                dynamic "http_headers" {
                  for_each = http_get.value["http_headers"]
                  iterator = http_header
                  content {
                    name  = http_header.value["name"]
                    value = http_header.value["value"]
                  }
                }
              }
            }
            dynamic "tcp_socket" {
              for_each = startup_probe.value["tcp_socket"] != null ? [startup_probe.value["tcp_socket"]] : []
              content {
                port = tcp_socket.value["port"]
              }
            }
            dynamic "grpc" {
              for_each = startup_probe.value["grpc"] != null ? [startup_probe.value["grpc"]] : []
              content {
                port    = grpc.value["port"]
                service = grpc.value["service"]
              }
            }
          }
        }
      }
    }
    dynamic "volumes" {
      for_each = var.volumes
      iterator = volume
      content {
        name = volume.value["name"]
        dynamic "secret" {
          for_each = volume.value["secret"] != null ? [volume.value["secret"]] : []
          content {
            secret       = secret.value["secret"]
            default_mode = secret.value["default_mode"]
            dynamic "items" {
              for_each = length(secret.value["items"]) >= 1 ? secret.value["items"] : []
              iterator = item
              content {
                path    = item.value["path"]
                version = item.value["version"]
                mode    = item.value["mode"]
              }
            }
          }
        }
        dynamic "cloud_sql_instance" {
          for_each = volume.value["cloud_sql_instance"] != null ? [volume.value["cloud_sql_instance"]] : []
          content {
            instances = cloud_sql_instance.value["instances"]
          }
        }
      }
    }
    dynamic "volumes" {
      for_each = var.mount_cloudsql_instance != null ? [1] : []
      iterator = instance
      content {
        name = "cloudsql"
        cloud_sql_instance {
          instances = [var.mount_cloudsql_instance]
        }
      }
    }
  }
  dynamic "traffic" {
    for_each = var.traffic
    content {
      type     = traffic.value["type"]
      revision = traffic.value["revision"]
      percent  = traffic.value["percent"]
      tag      = traffic.value["tag"]
    }
  }
  depends_on = [
    null_resource.pre_deploy_job_trigger
  ]
}

# Allow unauthenticated invocations for the webapp.
resource "google_cloud_run_v2_service_iam_member" "webapp_all_users_invoker" {
  count = var.allow_unauthenticated_invocations ? 1 : 0

  location = google_cloud_run_v2_service.webapp.location
  project  = google_cloud_run_v2_service.webapp.project
  name     = google_cloud_run_v2_service.webapp.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

module "uptime_monitoring" {
  for_each = local.monitor_hosts

  source  = "gitlab.developers.cam.ac.uk/uis/gcp-site-monitoring/devops"
  version = "~> 5.0"

  host                        = each.value.host
  project                     = var.project
  monitoring_scoping_project  = coalesce(var.monitoring_scoping_project, var.project)
  alert_notification_channels = var.alerting_notification_channels
  local_files_dir             = var.local_files_dir

  uptime_check = {
    # Accept either e.g. "60s" or 60 for timeout and periods for compatibility
    # with previous releases.
    timeout                   = tonumber(trimsuffix(var.alerting_uptime_timeout, "s"))
    period                    = tonumber(trimsuffix(var.alerting_uptime_period, "s"))
    path                      = var.monitoring_path
    success_threshold_percent = var.alerting_success_threshold_percent

    alert_enabled = var.enable_alerting
  }

  tls_check = {
    alert_enabled = var.enable_alerting
  }

  # If required, configure the monitoring to use an authentication proxy, allowing
  # the monitoring checks to invoke the cloud run instance.
  authentication_proxy = {
    enabled                     = each.value.enable_auth_proxy
    cloud_run_project           = google_cloud_run_v2_service.webapp.project
    cloud_run_service_name      = google_cloud_run_v2_service.webapp.name
    cloud_run_region            = var.region
    egress_connector            = each.value.enable_egress_connector && local.auth_proxy_vpc_access != null ? local.auth_proxy_vpc_access.connector : null
    egress_connector_settings   = each.value.enable_egress_connector && local.auth_proxy_vpc_access != null ? local.auth_proxy_vpc_access.egress : null
    source_bucket_force_destroy = true
  }
}

# Configure a Cloud Run Job which will be executed before the deployment of the google_cloud_run_v2_service.webapp
# resource. This is primarily useful to run database migrations, however other use cases may exist.
resource "google_cloud_run_v2_job" "pre_deploy" {
  count = var.enable_pre_deploy_job ? 1 : 0

  name                = "${var.name}-pre-deploy"
  location            = var.region
  project             = var.project
  launch_stage        = var.pre_deploy_job_launch_stage
  deletion_protection = var.deletion_protection

  template {
    labels      = var.pre_deploy_job_labels
    annotations = var.pre_deploy_job_annotations
    parallelism = var.pre_deploy_job_parallelism
    task_count  = var.pre_deploy_job_task_count
    template {
      service_account       = google_service_account.webapp.email
      timeout               = var.pre_deploy_job_timeout
      execution_environment = var.pre_deploy_job_execution_environment
      encryption_key        = var.pre_deploy_job_encryption_key
      max_retries           = var.pre_deploy_job_max_retries

      containers {
        name        = var.pre_deploy_job_container.name
        image       = var.pre_deploy_job_container.image
        command     = var.pre_deploy_job_container.command
        args        = var.pre_deploy_job_container.args
        working_dir = var.pre_deploy_job_container.working_dir
        dynamic "env" {
          for_each = var.pre_deploy_job_container.env
          content {
            name  = env.value["name"]
            value = env.value["value"]
            dynamic "value_source" {
              for_each = env.value["value_source"] != null ? [env.value["value_source"]] : []
              content {
                dynamic "secret_key_ref" {
                  for_each = value_source.value["secret_key_ref"] != null ? [value_source.value["secret_key_ref"]] : []
                  content {
                    secret  = secret_key_ref.value["secret"]
                    version = secret_key_ref.value["version"]
                  }
                }
              }
            }
          }
        }
        dynamic "resources" {
          for_each = var.pre_deploy_job_container.resources != null ? [var.pre_deploy_job_container.resources] : []
          iterator = resource
          content {
            limits = resource.value["limits"]
          }
        }
        dynamic "ports" {
          for_each = var.pre_deploy_job_container.ports
          iterator = port
          content {
            name           = port.value["name"]
            container_port = port.value["container_port"]
          }
        }
        dynamic "volume_mounts" {
          for_each = var.pre_deploy_job_container.volume_mounts
          iterator = volume_mount
          content {
            name       = volume_mount.value["name"]
            mount_path = volume_mount.value["mount_path"]
          }
        }
        dynamic "volume_mounts" {
          for_each = var.pre_deploy_job_mount_cloudsql_instance != null ? [1] : []
          iterator = instance
          content {
            name       = "cloudsql"
            mount_path = "/cloudsql"
          }
        }
      }
      dynamic "volumes" {
        for_each = var.pre_deploy_job_volumes
        iterator = volume
        content {
          name = volume.value["name"]
          dynamic "secret" {
            for_each = volume.value["secret"] != null ? [volume.value["secret"]] : []
            content {
              secret       = secret.value["secret"]
              default_mode = secret.value["default_mode"]
              dynamic "items" {
                for_each = secret.value["items"]
                iterator = item
                content {
                  path    = item.value["path"]
                  version = item.value["version"]
                  mode    = item.value["mode"]
                }
              }
            }
          }
          dynamic "cloud_sql_instance" {
            for_each = volume.value["cloud_sql_instance"] != null ? [volume.value["cloud_sql_instance"]] : []
            content {
              instances = cloud_sql_instance.value["instances"]
            }
          }
        }
      }
      dynamic "volumes" {
        for_each = var.pre_deploy_job_mount_cloudsql_instance != null ? [1] : []
        iterator = instance
        content {
          name = "cloudsql"
          cloud_sql_instance {
            instances = [var.pre_deploy_job_mount_cloudsql_instance]
          }
        }
      }
      dynamic "vpc_access" {
        for_each = var.pre_deploy_job_vpc_access != null ? [var.pre_deploy_job_vpc_access] : []
        content {
          connector = vpc_access.value["connector"]
          egress    = vpc_access.value["egress"]
          dynamic "network_interfaces" {
            for_each = vpc_access.value["network_interfaces"] != null ? [vpc_access.value["network_interfaces"]] : []
            iterator = network_interface
            content {
              network    = network_interface.value["network"]
              subnetwork = network_interface.value["subnetwork"]
            }
          }
        }
      }
    }
  }
}

# Trigger the pre-deploy job using the gcloud CLI whenever the var.pre_deploy_job_container.image value changes.
resource "null_resource" "pre_deploy_job_trigger" {
  count = var.enable_pre_deploy_job && var.pre_deploy_job_trigger ? 1 : 0

  triggers = merge({
    image_name = var.pre_deploy_job_container.image
    }, var.pre_deploy_job_force ? {
    timestamp = timestamp()
  } : {})

  provisioner "local-exec" {
    command = <<EOI
gcloud --project ${var.project} run jobs execute \
  --region ${var.region} --wait ${google_cloud_run_v2_job.pre_deploy[0].name}
EOI

    environment = {
      # This environment variable tells gcloud CLI to authenticate using an access token. We're using the access token
      # configured in the default google provider via the google_client_config data source.
      CLOUDSDK_AUTH_ACCESS_TOKEN = data.google_client_config.current.access_token
    }
  }

  depends_on = [
    google_cloud_run_v2_job.pre_deploy
  ]
}
