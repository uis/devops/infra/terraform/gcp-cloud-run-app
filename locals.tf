# locals.tf defines common expressions used by the module.

locals {
  # Project containing existing Cloud SQL instance.
  sql_instance_project = coalesce(var.sql_instance_project, var.project)

  ingress = var.enable_load_balancer && var.ingress == null ? "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER" : (
    var.ingress == null ? "INGRESS_TRAFFIC_ALL" : var.ingress
  )

  # Whether we should monitor the custom domain - only possible if there are a dns names set and unauthenticated
  # invocation is enabled.
  can_monitor_custom_dns = length(var.dns_names) > 0 && var.allow_unauthenticated_invocations

  create_vpc_connector = var.vpc_access == null && (
    var.enable_static_egress_ip || (var.enable_monitoring && local.ingress != "INGRESS_TRAFFIC_ALL")
  )

  # Determines which VPC connector should be used for the Cloud Run service.
  vpc_access = local.create_vpc_connector ? {
    connector          = google_vpc_access_connector.main[0].id
    egress             = "ALL_TRAFFIC"
    network_interfaces = null
  } : var.vpc_access

  auth_proxy_vpc_access = local.create_vpc_connector ? {
    connector          = google_vpc_access_connector.main[0].id
    egress             = "ALL_TRAFFIC"
    network_interfaces = null
  } : var.vpc_access

  # Map containing the hosts to monitor and whether an auth proxy and egress vpc access connector should be configured.
  monitor_hosts = var.enable_monitoring ? merge({
    default = {
      host                    = trimsuffix(trimprefix(google_cloud_run_v2_service.webapp.uri, "https://"), "/"),
      enable_auth_proxy       = var.allow_unauthenticated_invocations == false || local.ingress != "INGRESS_TRAFFIC_ALL",
      enable_egress_connector = local.ingress != "INGRESS_TRAFFIC_ALL"
    },
    },
    local.can_monitor_custom_dns ? {
      for k, v in var.dns_names :
      k => {
        host                    = v
        enable_auth_proxy       = local.ingress == "INGRESS_TRAFFIC_INTERNAL_ONLY",
        enable_egress_connector = local.ingress == "INGRESS_TRAFFIC_INTERNAL_ONLY"
      }
    } : {}
  ) : {}
}
