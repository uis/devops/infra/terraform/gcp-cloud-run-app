# Known issues

## Manual Deletion Required for Static IP Address

`prevent_deletion` attribute is enabled for the
`google_compute_address.static_ip` resource. This attribute is used to prevent
accidental deletion of important resources. It is generally not expected for
static IP addresses to be deleted or recreated frequently, as these are often
referenced in other external configurations such as firewall rules and DNS
records.

with `prevent_deletion` applied, Terraform will display error trying to destroy
the resource:

```sh
│ Error: Instance cannot be destroyed
│
│   on .terraform/modules/webapp/static_egress_ip.tf line 40:
│   40: resource "google_compute_address" "static_ip" {
│
│ Resource module.webapp.google_compute_address.static_ip[0] has
│ lifecycle.prevent_destroy set, but the plan calls for this resource to be
│ destroyed. To avoid this error and continue with the plan, either disable
│ lifecycle.prevent_destroy or reduce the scope of the plan using
│ the -target flag.
```

However, sometimes it needs to be deleted. If a resource is created with
`prevent_deletion`, Terraform will block any attempt to delete this resource
through Terraform commands. In some situations, it is necessary to delete
static IP Address entries. In order to do this with `prevent_deletion` enabled,
you must first manually delete the resource via the console or `gcloud` cli
tool before running `terraform apply`.

A full example of this is shown below.

## Manually deleting a static IP address with `prevent_deletion` attribute

1. Confirm the name of the Cloud Router using the `gcloud` command:

   ```sh
   gcloud compute routers list --project $PROJECT_ID

   NAME              REGION        NETWORK
   webapp-ip-router  europe-west2  default
   ```

2. Delete the Cloud Router:

   ```sh
   gcloud compute routers delete [CLOUD_ROUTER]  --project $PROJECT_ID --region $REGION
   ```

3. Delete the IP address:

   ```sh
   gcloud compute addresses delete [IP_ADDRESS] --region $REGION --project $PROJECT_ID
   ```

4. Verify that the IP address is deleted:

   ```sh
   gcloud compute addresses list --project $PROJECT_ID
   ```

That's it. Now Terraform commands will work without errors.
