# Testing

This project makes use of Terraform's built-in `test` command to run integration
tests. The tests are configured in the `tests` directory and deploy resources to
our dedicated Infra Testing GCP project.

## Running tests locally

To run tests locally you can use the `run_tests.sh` helper script. By default
the script will execute _all_ tests, however, it is often useful to target a
specific test file using the `-t` option. For example, to run the
`cloud_run_service.tftest.hcl` test you would use the following command.

```bash
# You must have authenticated and set the application-default credentials.
gcloud auth application-default login

./run_tests.sh -t tests/cloud_run_service.tftest.hcl
```

Note that the `test` service defined in `docker-compose.yml` sets the
`GOOGLE_IMPERSONATE_SERVICE_ACCOUNT` variable. You must have permission to
impersonate this service account to be able to run these tests.

## GitLab CI/CD test jobs

The `tests` job in the `.gitlab-ci.yml` file is configured to run all of the
test files in the `tests` directory. The tests are run in parallel using a
`matrix` job. Variables are used to allow us to specify version constraints to
test against multiple supported major versions of the Google Terraform provider.

The `tests` job is configured to require a manual trigger. This is due to the
number of resources that the jobs will deploy and the length of time the jobs
take to complete. With this in mind, you should generally only need to run the
tests job at the point you open a merge request, ensuring the job is passing
before requesting a review.

## Resource teardown and cleanup

If a test job fails Terraform attempts to teardown any resources it has already
created. This seems to work well the majority of the time. However, to protect
against resources not being destroyed, and potentially costing £££, there are
two `cleanup` jobs configured in the `.gitlab-ci.yml` file, `pre-cleanup` and
`post-cleanup`. These jobs both run the `tests/cleanup.sh` script which is
configured to check for any resources that _could_ have been created and delete
any that it finds. We run the `pre-cleanup` job to ensure that there is a clean
environment prior to the current test run, avoiding any subnet clashes etc.

It's also fine to run the `tests/cleanup.sh` script from your local machine to
perform an ad-hoc cleanup. First authenticate your `gcloud` session and then
simply run the script, for example:

```bash
gcloud auth login

./tests/cleanup.sh
```

## Troubleshooting

### Google's eventually consistent APIs

Many of Google's APIs are eventually consistent. This often causes issues as IAM
bindings and API enablement can be delayed causing our Terraform to fail.
Unfortunately, this is simply unavoidable and the only workaround is to rerun
the failed job.

#### Error 403: Permission 'iam.serviceaccounts.actAs' denied on service account

The following error is an example of the eventual consistency issue. If you're
unlucky enough to see this failure you should simply retry the job as often it
just works the second time.

```bash
tests/cloud_run_service.tftest.hcl... in progress
  run "setup"... pass
  run "test_service_with_default_variable_values"... fail
╷
│ Error: Error creating Service: googleapi: Error 403: Permission
│ 'iam.serviceaccounts.actAs' denied on service account
│ test-fab59940-run@infra-testing-int-e2395220.iam.gserviceaccount.com
│ (or it may not exist).
│
│   with google_cloud_run_v2_service.webapp,
│   on main.tf line 23, in resource "google_cloud_run_v2_service" "webapp":
│   23: resource "google_cloud_run_v2_service" "webapp" {
│
╵
```

### Invalid IPCidrRange: 10.124.0.0/28 conflicts with existing subnetwork

This error usually means a previous test run failed to tear down its resources
correctly so the subnet range is already in use. You should investigate the
previous test runs and destroy all orphaned resources before rerunning the
failed job.

```bash
tests/monitoring.tftest.hcl... in progress
  run "setup"... pass
  run "test_monitoring_with_alert_policies_created_in_default_project"... pass
  run "test_monitoring_with_alert_policies_created_in_scoping_project"... pass
  run "test_monitoring_with_auth_proxy"... pass
  run "test_monitoring_with_auth_proxy_and_vpc_access_connector"... fail
╷
│ Error: Error waiting to create Subnetwork: Error waiting for Creating
│ Subnetwork: Invalid IPCidrRange: 10.124.0.0/28 conflicts with existing
│ subnetwork 'test-209abb96-vpc-connector' in region 'europe-west2'.
│
│
│   with google_compute_subnetwork.vpc_connector[0],
│   on static_egress_ip.tf line 6, in resource "google_compute_subnetwork"
│ "vpc_connector":
│    6: resource "google_compute_subnetwork" "vpc_connector" {
│
╵
```
