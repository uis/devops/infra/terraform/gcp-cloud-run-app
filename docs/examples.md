# Example Usage

This page contains some examples of the different ways this module can be
configured.

## Basic

A basic Cloud Run service with a single container definition.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }
}
```

## Load balancer

A basic Cloud Run service configured to use a load balancer for ingress.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }

  enable_load_balancer = true

  dns_names = {
    webapp = "webapp.test.example.com"
  }
}

resource "google_dns_record_set" "load_balancer_webapp" {
  name         = "webapp.test.example.com."
  type         = "A"
  ttl          = 300
  managed_zone = "example-zone"
  project      = "example-project-id-1234"

  rrdatas = [
    module.webapp.load_balancer.external_ip
  ]
}
```

## Secret environment variables and volumes

A Cloud Run service configured to load environment variables and mount volumes
via Google Secret Manager secret objects.

Note that you need to grant the created service account identity the
ability to access the secret objects _outside_ of this module call.

```hcl
resource "google_secret_manager_secret" "main" {
  secret_id = "my-secret"
  project   = "example-project-id-1234"

  replication {
    auto {}
  }
}

resource "google_secret_manager_secret_version" "main" {
  secret      = google_secret_manager_secret.main.id
  secret_data = "my-secret-data"
}

resource "google_secret_manager_secret_iam_member" "main" {
  project   = "example-project-id-1234"
  secret_id = google_secret_manager_secret.main.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${module.webapp.service_account.email}"
}

module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
      env = [
        {
          name = "SECRET",
          value_source = {
            secret_key_ref = {
              secret  = google_secret_manager_secret.main.id
              version = "latest"
            }
          }
        }
      ]
      volume_mounts = [
        {
          name       = "secret-volume",
          mount_path = "/secrets"
        }
      ]
    }
  }
  volumes = [
    {
      name = "secret-volume",
      secret = {
        secret = google_secret_manager_secret.main.id
        items = [
          {
            version = "latest",
            path    = "my-secret"
          }
        ]
      }
    }
  ]
}
```

## Mounting CloudSQL instances

A Cloud Run service which mounts an existing CloudSQL instance using the
`mount_cloudsql_instance` helper variable.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }

  mount_cloudsql_instance = module.sql.instance_connection_name
}

module "sql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "~> 23.0"

  database_version            = "POSTGRES_15"
  name                        = "test-sql-1234"
  project_id                  = "example-project-id-1234"
  tier                        = "db-f1-micro"
  availability_type           = "ZONAL"
  region                      = "europe-west2"
  zone                        = "europe-west2-a"
  deletion_protection         = false
  deletion_protection_enabled = false
}
```

## Pre-deploy job

A Cloud Run service with a corresponding "pre-deploy" Cloud Run job. See the
[Pre-deploy Cloud Run Job](../README.md#pre-deploy-cloud-run-job) section in the
README.md for more information.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  containers = {
    webapp = {
      image = "registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:5.0-py3.12"
    }
  }

  mount_cloudsql_instance = module.sql.instance_connection_name

  enable_pre_deploy_job = true

  pre_deploy_job_container = {
    image   = "registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:5.0-py3.12"
    command = ["python3"]
    args    = ["/usr/src/app/manage.py", "migrate"]
  }

  pre_deploy_job_mount_cloudsql_instance = module.sql.instance_connection_name
}

module "sql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "~> 23.0"

  database_version            = "POSTGRES_15"
  name                        = "test-sql-1234"
  project_id                  = "example-project-id-1234"
  tier                        = "db-f1-micro"
  availability_type           = "ZONAL"
  region                      = "europe-west2"
  zone                        = "europe-west2-a"
  deletion_protection         = false
  deletion_protection_enabled = false
}
```

## Multi-container deployment

A Cloud Run service which defines multiple containers (sidecars). For more
information see the [Cloud Run
documentation](https://cloud.google.com/run/docs/deploying#sidecars).

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-test-b99f7ad6"

  containers = {
    webapp1 = {
      name  = "webapp-1"
      image = "us-docker.pkg.dev/cloudrun/container/hello"
      ports = [
        {
          container_port = 8080
        }
      ]
    }
    webapp2 = {
      name  = "webapp-2"
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }
}
```

## Canary release traffic distribution

A Cloud Run service which allocates incoming traffic equally between two
revisions.

This example uses the `revision` variable to deploy named revisions of the Cloud
Run service. This allows you to target these named revisions specifically to
split traffic between one or more revisions via the `traffic` variable.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  revision = "v1-1-0"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }

  traffic = [
    {
      type     = "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
      revision = "v1-0-0"
      percent  = 50
    },
    {
      type     = "TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION"
      revision = "v1-1-0"
      percent  = 50
    }
  ]
}
```

## Static egress IP configuration

A Cloud Run service configured with a static IP address for egress. See the
[Static Outbound IP
Address](https://cloud.google.com/run/docs/configuring/static-outbound-ip) page
in the Cloud Run documentation for details of this implementation.

The address is available in the `static_egress_ip` output of this module.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  enable_static_egress_ip = true
  region                  = "europe-west2"
  project                 = "example-test-b99f7ad6"

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }
}
```

## Uptime and SSL Monitoring

A basic Cloud Run service with default monitoring enabled.

```hcl
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  region  = "europe-west2"
  project = "example-project-id-1234"

  enable_monitoring = true

  containers = {
    webapp = {
      image = "us-docker.pkg.dev/cloudrun/container/hello"
    }
  }
}
```
