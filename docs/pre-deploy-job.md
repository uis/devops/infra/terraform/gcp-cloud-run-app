# Pre-deploy Cloud Run job

The `8.0.0` release introduced the `enable_pre_deploy_job` variable which, when
set to `true`, creates a Cloud Run job to execute a configurable command before
new Cloud Run service revisions are deployed. This is a useful way to run
database migrations and other commands which are tightly coupled to the release
cadence of the main Cloud Run service.

The pre-deploy job is configured via the `pre_deploy_job_*` variables which can
be found in `variables.tf`.

## Triggering the Cloud Run job

The Cloud Run job is executed by a `null_resource` resource which simply runs
the `gcloud run jobs execute` command. The `null_resource` is triggered each
time the `var.pre_deploy_job_container.image` value changes by default, although
you can force it to run via the `pre_deploy_job_force` variable.

## Order of operations

To ensure that the pre-deploy job runs before a new revision of the Cloud Run
service is deployed, the resources in question are explicitly configured with
`depends_on` relationships as follows.

- The `google_cloud_run_v2_job.pre_deploy` Cloud Run job has no `depends_on`
  relationships defined and is therefore deployed first.
- The `null_resource.pre_deploy_job_trigger` resource depends on
  `google_cloud_run_v2_job.pre_deploy` and therefore won't be deployed until the
  Cloud Run job is deployed successfully.
- Finally, the `google_cloud_run_service.webapp` Cloud Run service depends on
  `null_resource.pre_deploy_job_trigger`, meaning it is only deployed once the
  `null_resource.pre_deploy_job_trigger` has executed successfully.
