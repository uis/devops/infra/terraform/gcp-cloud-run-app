# GCP Cloud Run Terraform Module

This module manages the deployment of containerised applications on Cloud Run.
It includes the following functionality:

- Creation of the main Cloud Run service.
- Creation of a dedicated service account with required IAM bindings.
- Deployment of an optional load balancer configuration.
- Deployment of an optional "pre-deployment" Cloud Run job to handle tasks such
  as database migrations.
- Deployment of an optional static egress IP address for the Cloud Run service.
- Configuration of simple uptime checks and SSL certificate expiry alerts.
- Convenience variables to configure certain aspects of the Cloud Run service
  which would otherwise be complex, such as mounting Cloud SQL instances.

## Cloud Run v2 API

Following release `9.0.0`, this module has been refactored to use the Cloud Run
`v2` API resources exclusively (i.e. `google_cloud_run_v2_service`). This means
that many input variables are now different and updating from a previous version
will require some changes to your module definition. See the guidebook for our
[migration
guide](https://guidebook.devops.uis.cam.ac.uk/howtos/migrate-a-gcp-cloud-run-app-terraform-deployment-to-v9/#update-the-module-version).

## Examples

See the [docs/examples.md](docs/examples.md) page for a full list of detailed
usage examples.

## Pre-deploy Cloud Run Job

The `8.0.0` release introduced the `enable_pre_deploy_job` variable which, when
set to `true`, creates a Cloud Run job to execute a configurable command before
new Cloud Run service revisions are deployed. This is a useful way to run
database migrations and other commands which are tightly coupled to the release
cadence of the main Cloud Run service.

The pre-deploy job is configured via the `pre_deploy_job_*` variables which can
be found in `variables.tf`.

For more information on how the pre-deploy Cloud Run job works see the
[pre-deploy-job.md](../docs/pre-deploy-job.md) page.
