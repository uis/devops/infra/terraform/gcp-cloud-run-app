output "service" {
  description = "Webapp Cloud Run service resource"
  value       = google_cloud_run_v2_service.webapp
}

output "service_account" {
  description = "Service account which service runs as"
  value       = google_service_account.webapp
}

output "network_endpoint_group" {
  description = "Network endpoint group for the load balancer."
  value       = try(google_compute_region_network_endpoint_group.webapp[0], null)
}

output "ssl_policy" {
  description = "The ssl_policy object, if one is being created."
  value       = try(google_compute_ssl_policy.default[0], null)
}

output "load_balancer" {
  description = "Load balancer for the webapp"
  value       = try(module.webapp_http_load_balancer[0], null)
  sensitive   = true
}

output "static_egress_ip" {
  description = <<EOI
The static egress IP assigned to this cloud run instance. Only populated
if the variable `enable_static_egress_ip` is true.
EOI
  value       = var.enable_static_egress_ip ? google_compute_address.static_ip[0].address : ""
}
